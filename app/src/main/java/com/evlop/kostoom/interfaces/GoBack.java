package com.evlop.kostoom.interfaces;

/**
 * Created by rachindrapoudel on 12/20/16.
 */

public interface GoBack {
    public void goBack();
    public void backToHome();
}
