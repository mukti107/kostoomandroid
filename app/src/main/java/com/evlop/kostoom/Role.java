package com.evlop.kostoom;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.evlop.kostoom.adapters.RoleSelectAdapter;
import com.evlop.kostoom.async.SetUserType;

/**
 * Created by rh on 12/6/16.
 */
public class Role extends AppCompatActivity implements View.OnClickListener {

    public static final String CUSTOMER ="customer" ;
    public static final String TAILOR ="tailor" ;
    public static final String SUPPLIER ="supplier" ;

    public static final int[] CUSTOMER_MENU=new int[]{R.id.nav_invoice,R.id.nav_offer};
    public static final int[] TAILOR_MENU=new int[]{R.id.nav_invoice,R.id.nav_order,R.id.nav_cart,R.id.nav_payment};
    public static final int[] SUPPLIER_MENU=new int[]{R.id.nav_cart,R.id.nav_payment,R.id.nav_order,R.id.nav_invoice};

//    Button customer,tailor,supplier;
    String username,password,facebookToken;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.role);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window =getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorAccent));
        }

        Bundle extra=getIntent().getExtras();
        if(extra!=null) {
            username = extra.getString("username");
            password = extra.getString("password");
            facebookToken = extra.getString("token");
        }

        PagerTabStrip stripe=(PagerTabStrip) findViewById(R.id.role_title);
        stripe.setTextColor(getResources().getColor(R.color.colorAccent));
        stripe.setTabIndicatorColor(getResources().getColor(R.color.colorAccent));

        ViewPager vp=(ViewPager)findViewById(R.id.roles);
        vp.setAdapter(new RoleSelectAdapter(getSupportFragmentManager(),getApplication(),this));
        vp.setCurrentItem(0);
    }


    @Override
    public void onClick(View v) {
        String type=v.getTag().toString().toLowerCase();
//        switch (){
//            case R.id.role_customer:
//                type=CUSTOMER;
//                break;
//            case R.id.role_tailor:
//                type= TAILOR;
//                break;
//            case R.id.role_supplier:
//                type=SUPPLIER;
//                break;
//        }
        new SetUserType(this,username,type,password,facebookToken).execute();
    }

    public static void prepareMenu(String string, Menu menu) {
        int[] items=null;
        switch (string){
            case CUSTOMER:
                items=CUSTOMER_MENU;
                break;
            case TAILOR:
                items=TAILOR_MENU;
                break;
            case SUPPLIER:
                items=SUPPLIER_MENU;
                break;
        }

        for(int item:items){
            menu.removeItem(item);
        }

    }
}
