package com.evlop.kostoom.fragmets;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.components.AlertDialogActions;
import com.evlop.kostoom.components.KostoomAlertDialog;
import com.evlop.kostoom.functions.HttpTask;
import com.evlop.kostoom.functions.Util;
import com.evlop.kostoom.model.SewingOrder;
import com.michael.easydialog.EasyDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.evlop.kostoom.MainActivity.context;

/**
 * Created by rh on 12/10/16.
 */
public class Cart extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private List<SewingOrder> orders;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.cart);
        view=inflater.inflate(R.layout.cart,null);
        listOrders();
        return view;
    }

    private void listOrders() {

        LinearLayout ordersView=(LinearLayout)view.findViewById(R.id.cart_items);

        view.findViewById(R.id.checkout).setOnClickListener(this);

        int  totalItems=0;
        for(SewingOrder order:orders){
            totalItems+=order.getQuantity();
        }
//            ordersView.setAdapter(new OrderAdapter(getActivity(),orders));
//            ordersView.setOnItemClickListener(this);

        if(totalItems>0){
            view.findViewById(R.id.empty).setVisibility(View.GONE);
            ordersView.setVisibility(View.VISIBLE);
        }else{
            view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
            view.findViewById(R.id.place_order).setOnClickListener(this);
            ordersView.setVisibility(View.GONE);
            return;
        }


        ordersView.removeAllViews();
        for (final SewingOrder order:orders){
            Log.i("sizechartdata",order.getItem().getSizeChartData());
            try {
                order.getItem().setSizeChart(new JSONObject(order.getItem().getSizeChartData()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            View orderView=getActivity().getLayoutInflater().inflate(R.layout.order_item,null);
            ((TextView)orderView.findViewById(R.id.name)).setText(order.getItem().getName());
            ((TextView)orderView.findViewById(R.id.quantity)).setText(order.getQuantity()+" Pieces");
            ((TextView)orderView.findViewById(R.id.rate)).setText("Rp. "+Util.cf.format(order.getRateWithTransactionDiscount())+"/pieces");
            int furingPrice = order.getFuringPriceWithTransactionDiscount();
            ((TextView)orderView.findViewById(R.id.price)).setText(
                    Html.fromHtml(((furingPrice>0)?"Furing Price: "+Util.cf.format(furingPrice)+"<br/>":"")+
                            "Sub total: Rp. "+Util.cf.format(order.getNetPriceWithTransactionDiscount()+order.getFuringPriceWithTransactionDiscount())
                    ));
            ImageView itemImage=(ImageView)orderView.findViewById(R.id.image);
            itemImage.setImageBitmap(order.getImages().get(0).getBitmap());
            final View options= orderView.findViewById(R.id.options);
            ordersView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showOrderDetails(order);
                }
            });
            options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    View menuLayout= getActivity().getLayoutInflater().inflate(R.layout.cart_option_menu,null);


                    final EasyDialog ed=new EasyDialog(getActivity()).setLayout(menuLayout).setBackgroundColor(getActivity().getResources().getColor(android.support.v7.appcompat.R.color.background_material_light)).setLocationByAttachedView(options).setGravity(EasyDialog.GRAVITY_BOTTOM).setAnimationAlphaShow(300, 0.3f, 1.0f).setAnimationAlphaDismiss(300, 1.0f, 0.0f).setTouchOutsideDismiss(true).setMatchParent(false).setMarginLeftAndRight(5, 5).show();
                    menuLayout.findViewById(R.id.remove).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            deleteOrder(order);
                            ed.dismiss();
                        }
                    });

                    menuLayout.findViewById(R.id.edit).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            BusinessSewing sewingOrderFragment=new BusinessSewing();
                            sewingOrderFragment.setOrder(order);
                            ((MainActivity)getActivity()).openFargment(sewingOrderFragment);
                            ed.dismiss();
                        }
                    });
                }
            });
            //ImageLoader.getInstance().displayImage(getItem(i).getItem().getImageUrl(),itemImage);
           ordersView.addView(orderView);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.checkout:
                if(orders.size()==0)
                    Toast.makeText(getContext(),"There is no item in the cart",Toast.LENGTH_LONG).show();
                else
                    new FetchUserData().execute();
                break;
            case R.id.place_order:
                ((MainActivity)getActivity()).openHome();

        }
    }

    public void deleteOrder(final SewingOrder order){
        KostoomAlertDialog dialog=new KostoomAlertDialog().setActivity(getActivity());
        dialog.setMessage("Do you want to remove this order from cart?");
        dialog.setTitle("Remove from cart");

        dialog.setActions(new AlertDialogActions() {
            @Override
            public void onPositiveAction() {
                order.delete();
                Cart.this.orders.remove(order);
                listOrders();
                //getActivity().getSupportFragmentManager().beginTransaction().detach(Cart.this).attach(Cart.this).commit();
            }
            @Override public void onNegativeAction() {
            }
        });
        dialog.show();
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
        showOrderDetails(orders.get(i));
    }




    public void showOrderDetails(final SewingOrder order){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.order_summary, null);
        dialogView.findViewById(R.id.steps).setVisibility(View.GONE);
        ((Button)dialogView.findViewById(R.id.add_to_cart)).setText("Close");

        BusinessSewing.generateSummary(dialogView,order,getContext());

        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialogView.findViewById(R.id.add_to_cart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    public Cart withOrders(List<SewingOrder> orders) {
        this.orders=orders;
        return this;
    }

    private class FetchUserData extends AsyncTask<String, Void, String> {
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(getActivity(), "Processing", "Please Wait...", true);
        }

        @Override
        protected String doInBackground(String... strings) {
            String source= HttpTask.baseURL+"/user/details?token="+MainActivity.applicationData(getContext()).getString("user-token","");
            return HttpTask.get(source);
        }

        @Override
        protected void onPostExecute(String s) {
            progress.dismiss();

            try {
                JSONObject details=new JSONObject(s);
                showConfirmation(details.getString("name"),details.getString("phone"),details.getString("email"),details.getString("address1"),details.getString("city"),details.getString("address2"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private void showConfirmation(String name,String phone,String email,String address,String city,String district) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.checkout_confirm, null);
            dialogBuilder.setView(dialogView);
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            final EditText nameEdit,phoneEdit,emailEdit,editAddress,editCity,editDistrict;
            nameEdit= (EditText) dialogView.findViewById(R.id.name);
            phoneEdit= (EditText) dialogView.findViewById(R.id.phone);
            emailEdit= (EditText) dialogView.findViewById(R.id.email);
            editAddress= (EditText) dialogView.findViewById(R.id.address);
            editCity= (EditText) dialogView.findViewById(R.id.city);
            editDistrict= (EditText) dialogView.findViewById(R.id.district);
            if(name!="null")
            nameEdit.setText(name);
            if(phone!="null")
            phoneEdit.setText(phone);
            if(email!="null")
            emailEdit.setText(email);
            if(address!="null")
            editAddress.setText(address);
            if(city!="null")
            editCity.setText(city);
            if(district!="null")
            editDistrict.setText(district);


            View.OnClickListener listner=new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String[] values=new String[]{
                            nameEdit.getText().toString(),
                            phoneEdit.getText().toString(),
                            emailEdit.getText().toString(),
                            editAddress.getText().toString(),
                            editCity.getText().toString(),
                            editDistrict.getText().toString()
                    };

                    for (String s:values) {
                        if (s.trim().isEmpty()) {
                            Toast.makeText(getContext(), "Please fill all the fields", Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                    alertDialog.dismiss();

                }
            };
            dialogView.findViewById(R.id.confirm).setOnClickListener(listner);
            alertDialog.show();
        }
    }

}