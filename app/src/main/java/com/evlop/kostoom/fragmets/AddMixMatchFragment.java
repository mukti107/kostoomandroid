package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import com.evlop.kostoom.R;
import com.evlop.kostoom.adapters.ImageGrabber;
import com.evlop.kostoom.adapters.ImageUploadAdapter;
import com.evlop.kostoom.model.FabricMatch;
import com.evlop.kostoom.model.Image;

import java.util.ArrayList;

/**
 * Created by rachindrapoudel on 1/18/17.
 */
public class AddMixMatchFragment extends Fragment implements View.OnClickListener {

    private BusinessSewing businessSewing;
    EditText quantity,note;
    ArrayList<Image> images = new ArrayList<>();
    Spinner type;
    GridView gridView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.add_mix_match_record,null);
        quantity= (EditText) view.findViewById(R.id.quantity);
        type=(Spinner)view.findViewById(R.id.size_select);
        note=(EditText)view.findViewById(R.id.note);
        gridView=(GridView) view.findViewById(R.id.upload_fabric);
        gridView.setAdapter(new ImageUploadAdapter(getContext(),images));
        gridView.setOnItemClickListener(new ImageGrabber(businessSewing,images,gridView));
        //view.findViewById(R.id.add_mix_match).setOnClickListener(this);
        return view;
    }

    public Fragment withInstance(BusinessSewing businessSewing) {
        this.businessSewing=businessSewing;
        return this;
    }

    @Override
    public void onClick(View view) {

        if(images.size()==0){
            Toast.makeText(businessSewing.getContext(),R.string.warning_upload_fabric_image,Toast.LENGTH_SHORT).show();
            return;
        }

        if(Integer.parseInt("0"+quantity.getText().toString())<1){
            Toast.makeText(businessSewing.getContext(),R.string.warning_order_quantity,Toast.LENGTH_SHORT).show();
            return;
        }

        FabricMatch match=new FabricMatch();
        match.setQuantity(Integer.parseInt("0"+quantity.getText().toString()));
        match.setType(type.getSelectedItemPosition());
        match.setImages(images);
        match.setNote(note.getText().toString());
        quantity.setText("");
        type.setSelection(0);
        images=new ArrayList<>();
        note.setText("");
        gridView.setAdapter(new ImageUploadAdapter(getContext(),images));
        gridView.setOnItemClickListener(new ImageGrabber(businessSewing,images,gridView));
        businessSewing.order.getFabricMatch().add(match);
    }
}
