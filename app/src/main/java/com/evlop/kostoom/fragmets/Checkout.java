package com.evlop.kostoom.fragmets;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.async.CheckOut;
import com.evlop.kostoom.functions.HttpTask;
import com.evlop.kostoom.functions.Util;
import com.evlop.kostoom.interfaces.GoBack;
import com.evlop.kostoom.model.SewingOrder;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by rachindrapoudel on 1/26/17.
 */

public class Checkout extends Fragment implements View.OnClickListener,GoBack {

    int step=0;
    Fragment fragment;
    int[] stepImages=new int[]{R.id.cart,R.id.profile,R.id.address,R.id.schedule,R.id.payment,R.id.confirm};
    private View view;
    com.evlop.kostoom.model.Checkout data=new com.evlop.kostoom.model.Checkout();

    public static long distance=0;

    int totalItems=0,totalPrice=0,totalWithDiscount=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        data.setOrders(SewingOrder.listAll(SewingOrder.class));
        view=inflater.inflate(R.layout.checkout_process,container,false);
        updateFragment();
        view.findViewById(R.id.checkout_next).setEnabled(data.getOrders().size()>0);
        view.findViewById(R.id.checkout_next).setOnClickListener(this);
        distance=0;
        updateCartDetails();

        return view;
    }

    public int pickupPrice(){

        if(distance==0 || totalItems==0)
            return 0;

        if(totalItems<48){
            if(distance<10)
                return 10000;
            else if(distance>=10 && distance<25)
                return 15000;
            else if(distance>=25 && distance<40)
                return 25000;
            else if(distance>=40 && distance<=60)
                return 40000;
        }else{
            if(distance>=40 && distance<=60)
                return 30000;
        }
        return 0;
    }

    public void updateCartDetails(){
        totalItems=0;
        totalPrice=0;
        totalWithDiscount=0;
        for(SewingOrder order:data.getOrders()){
            totalItems+=order.getQuantity();
        }
        for(SewingOrder order:data.getOrders()){
            totalPrice+=order.getPrice(totalItems);
            totalWithDiscount+=order.getNetPriceWithTransactionDiscount()+order.getFuringPriceWithTransactionDiscount();
        }
        ((TextView)view.findViewById(R.id.cart_total)).setText(""+data.getOrders().size());
        ((TextView)view.findViewById(R.id.next)).setText(Html.fromHtml("Estimated Price: <br/><font color='red'>Rp. "+Util.cf.format(totalWithDiscount+pickupPrice())+"</font>"));

        if(totalItems<24){
            view.findViewById(stepImages[3]).setVisibility(View.GONE);
        }else{
            view.findViewById(stepImages[3]).setVisibility(View.VISIBLE);
        }
    }


    public void updateFragment(){


        for (int i:stepImages)
            view.findViewById(i).setAlpha((i==stepImages[step])?1f:0.5f);
        //((Button)view.findViewById(R.id.checkout_next)).setText(R.string.next);

        switch (step){
            case 0:
                fragment=new Cart().withOrders(data.getOrders());
                //((Button)view.findViewById(R.id.checkout_next)).setText(R.string.checkout);
                break;
            case 1:
                fragment=new ConfirmProfile();
                break;
            case 2:
                if(totalItems<24)
                    fragment=new DeliveryInfo();
                else
                    fragment=new Map().withLocation(data.getPickupLocation(),data.getDeliveryLocation());
                break;
            case 3:
                    fragment=new CheckoutSchedule().withData(data).withParent(this);

                break;
            case 4:
                fragment=new PaymentInfo();

                int downPayment= (int) (totalWithDiscount*0.3);
                if(downPayment%10000>0){
                    downPayment=((downPayment/10000)+1)*10000;
                }
                ((PaymentInfo)fragment).setDownPayment( downPayment);
                //((Button)view.findViewById(R.id.checkout_next)).setText(R.string.confirm_checkout);
                break;
            case 5:
                //((Button)view.findViewById(R.id.checkout_next)).setText(R.string.complete);
                fragment=new FinishCheckout();
        }
        getChildFragmentManager().beginTransaction().replace(R.id.checkout_content,fragment).commit();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.checkout_next:
                switch (step){
                    case 1:
                        if(!((ConfirmProfile)fragment).isValid())
                            return;
                        data.setDetails(((ConfirmProfile)fragment).getDetails());
                        break;
                    case 2:
                        if(fragment instanceof Map) {
                            data.setPickupLocation(((Map) fragment).getPickupLocation());
                            data.setDeliveryLocation(((Map) fragment).getDeliveryLocation());
                        }
                        break;
                    case 3:
                        data.setDate(((CheckoutSchedule)fragment).getDate());
                        data.setTime(((CheckoutSchedule)fragment).getTime());
                        data.setVehicle(((CheckoutSchedule)fragment).getVehicle());
                        break;
                    case 4:
                        new ProcessCheckout(getActivity(),data).execute();
                        return;
                    case 5:
                        ((MainActivity)getActivity()).openHome();
                }
                if(step<4) {
                    if(totalItems<24 && step==2)
                        step=4;
                    else
                        step++;
                    updateFragment();
                }
        }
    }

    @Override
    public void goBack() {

        if(totalItems<24 && step==4) {
            step = 2;
            updateFragment();
        }
        else if(step>0 && step<5) {
            step--;
            updateFragment();
        }else {
            ((MainActivity)getActivity()).openHome();
        }
    }

    @Override
    public void backToHome() {

    }

    private class ProcessCheckout extends CheckOut {
        public ProcessCheckout(Context context, com.evlop.kostoom.model.Checkout checkout) {
            super(context, checkout);
        }

        @Override
        protected void showSuccess(){
            step++;
            updateFragment();
        }
    }
}
