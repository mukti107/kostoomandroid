package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.evlop.kostoom.R;
import com.evlop.kostoom.functions.Util;

/**
 * Created by rachindrapoudel on 1/27/17.
 */
public class DeliveryInfo extends android.support.v4.app.Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Send Fabrics");
        View view=inflater.inflate(R.layout.delivery_info,null);
        return view;
    }
}
