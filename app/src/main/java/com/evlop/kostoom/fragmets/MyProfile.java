package com.evlop.kostoom.fragmets;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.evlop.kostoom.Constants;
import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.async.*;
import com.evlop.kostoom.interfaces.GoBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by rh on 12/10/16.
 */
public class MyProfile extends Fragment implements View.OnClickListener {

    private JSONObject accountDetails;
    TextView name,email,phone;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.my_account);
        View view=inflater.inflate(R.layout.my_profile,null);
        name= (TextView) view.findViewById(R.id.name);
        email= (TextView) view.findViewById(R.id.email);
        phone= (TextView) view.findViewById(R.id.phone);

        view.findViewById(R.id.edit).setOnClickListener(this);
        view.findViewById(R.id.privacy_policy).setOnClickListener(this);
        view.findViewById(R.id.rate).setOnClickListener(this);
        view.findViewById(R.id.change_password).setOnClickListener(this);
        view.findViewById(R.id.change_language).setOnClickListener(this);

        view.findViewById(R.id.save).setOnClickListener(this);

        try {
            name.setText(accountDetails.getString("name"));
            email.setText(accountDetails.getString("email"));
            if(accountDetails.getString("phone")!="null")
                phone.setText(accountDetails.getString("phone"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.edit:
                new com.evlop.kostoom.async.MyAccount((MainActivity) getActivity()).setProfile(false).execute();
                break;
            case R.id.privacy_policy:

                new AlertDialog.Builder(getActivity())
                        .setTitle("Privacy Policy")
                        .setMessage(R.string.privacy_policy)
                        .setCancelable(false)
                        .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();

                break;
            case R.id.rate:
                final String appPackageName = getActivity().getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;
            case R.id.change_password:
                ((MainActivity)getActivity()).openFargment(new ChangePassword());
                break;
            case R.id.change_language:
                changeLanguage();
                break;


        }

    }

    public Fragment with(JSONObject accountDetails) {
        this.accountDetails=accountDetails;
        return this;
    }

    public void changeLanguage() {
        final CharSequence[] items = {"English", "Indonesian",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                String lang="";
                if (items[item].equals("English")) {
                    lang="en";
                } else if (items[item].equals("Indonesian")) {
                    lang="in";
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }

                if(!lang.isEmpty()) {
                    MainActivity.applicationData(getContext()).edit().putString("langage", lang).apply();
                    Resources res = getResources();
                    DisplayMetrics dm = res.getDisplayMetrics();
                    android.content.res.Configuration conf = res.getConfiguration();
                    conf.locale = new Locale(lang);
                    res.updateConfiguration(conf, dm);
                }

            }
        });
        builder.show();
    }
}
