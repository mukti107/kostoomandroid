package com.evlop.kostoom.fragmets;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.evlop.kostoom.R;
import com.evlop.kostoom.adapters.ImageGrabber;
import com.evlop.kostoom.adapters.ImageUploadAdapter;
import com.evlop.kostoom.model.FabricMatch;
import com.evlop.kostoom.model.Image;

import java.util.ArrayList;

/**
 * Created by rachindrapoudel on 1/18/17.
 */
public class MixMatchSummaryFragment extends Fragment {

    private BusinessSewing businessSewing;
    LinearLayout summaryContainer;
    private LayoutInflater inflater;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.mix_match_record,null);
        summaryContainer=(LinearLayout) v.findViewById(R.id.mix_match_summary_container);
        this.inflater=inflater;
        populateMixMatch(summaryContainer,businessSewing.order.getFabricMatch());
        return v;
    }

    public Fragment withInstane(BusinessSewing businessSewing) {
        this.businessSewing=businessSewing;
        return this;
    }

    private void populateMixMatch(LinearLayout mixMatch, ArrayList<FabricMatch> mixAndMatch) {
        mixMatch.removeAllViews();
        for (final FabricMatch match:mixAndMatch){
            View v=inflater.inflate(R.layout.summary_mix_match_record,null);
            GridView gridView=(GridView) v.findViewById(R.id.upload_fabric);

            gridView.setAdapter(new ImageUploadAdapter(getContext(),match.getImages()).setUploadable(false));

            TextView quantity= (TextView) v.findViewById(R.id.quantity);
            quantity.setText(""+match.getQuantity());

            TextView size= (TextView) v.findViewById(R.id.size_select);
            size.setText(getResources().getStringArray(R.array.size_list)[match.getType()]);

            TextView note= (TextView) v.findViewById(R.id.note);
            if(!match.getNote().trim().isEmpty())
                note.setText(match.getNote());
            else
                note.setText("N/A");
            mixMatch.addView(v);
        }
    }

}
