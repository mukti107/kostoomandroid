package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.adapters.FeedsAdapter;
import com.evlop.kostoom.async.FetchOrder;
import com.evlop.kostoom.async.ProjectFeed;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by rh on 12/10/16.
 */
public class MyOffers extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    static JSONArray feeds=null;
    private SwipeRefreshLayout refresh;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.my_offers);
        if(feeds==null) {
            new com.evlop.kostoom.async.MyOffers((MainActivity) getActivity()).execute();
            return null;
        }
        View view=inflater.inflate(R.layout.project_feeds,null);

        refresh=(SwipeRefreshLayout)view.findViewById(R.id.sync);
        refresh.setOnRefreshListener(this);

        ListView feedsView=(ListView)view.findViewById(R.id.order_feeds);
        feedsView.setAdapter(new FeedsAdapter((MainActivity) getActivity(),feeds));
        feedsView.setOnItemClickListener(this);
        return view;
    }

    public MyOffers with(JSONArray feeds){
        this.feeds=feeds;
        return this;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.sync:
                new com.evlop.kostoom.async.MyOffers((MainActivity) getActivity()).execute();
                break;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        try {
            new FetchOrder((MainActivity) getActivity(),feeds.getJSONObject(i).getString("id")).withReturn(MyOffers.this).execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        new com.evlop.kostoom.async.MyOffers((MainActivity) getActivity()).execute();

        refresh.setRefreshing(false);
    }
}
