package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.adapters.ItemListAdapter;
import com.evlop.kostoom.async.FetcProducts;
import com.evlop.kostoom.components.ImageSlider;
import com.evlop.kostoom.functions.Util;
import com.evlop.kostoom.model.Product;
import com.evlop.kostoom.model.SewingOrder;

import java.util.ArrayList;

/**
 * Created by rh on 12/10/16.
 */
public class PriceList extends Fragment {

    static ArrayList<Product> products=null;
    SwipeRefreshLayout refresh;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getActivity().setTitle(R.string.price_list);

        if(products==null) {
            new FetcProducts((MainActivity) getActivity()).showIn(this).execute();
            return null;
        }
        View view=inflater.inflate(R.layout.product_price_list,null);
        TableLayout items= (TableLayout) view.findViewById(R.id.price_list);
        for(Product p:products) {
            View row = inflater.inflate(R.layout.product_price_list_row, null);
            ((TextView)row.findViewById(R.id.name)).setText(p.getName());
            ((TextView)row.findViewById(R.id.unit_price)).setText("Rp. "+ Util.cf.format(p.getPrice().getP1()));
            ((TextView)row.findViewById(R.id.business_price)).setText("Rp. "+ Util.cf.format(p.getPrice().getP2()));
            items.addView(row);
        }
        //items.setAdapter(new ItemListAdapter(getActivity().getApplicationContext(),products));
        return view;
    }


    public Fragment withProducts(ArrayList<Product> productList) {
        this.products=productList;
        return this;
    }


}
