package com.evlop.kostoom.fragmets;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.adapters.ItemListAdapter;
import com.evlop.kostoom.async.SaveMyAccount;
import com.evlop.kostoom.interfaces.GoBack;
import com.evlop.kostoom.model.Product;
import com.evlop.kostoom.model.SewingOrder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by rh on 12/10/16.
 */
public class MyAccount extends Fragment implements View.OnClickListener,  DatePickerDialog.OnDateSetListener,GoBack {

    private JSONObject accountDetails;
    EditText name,email,phone,brand,address,city,district,dob;
    Spinner gender;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.profile_edit);
        View view=inflater.inflate(R.layout.my_account,null);
        name= (EditText) view.findViewById(R.id.name);
        email= (EditText) view.findViewById(R.id.email);
        phone= (EditText) view.findViewById(R.id.phone);
        brand= (EditText) view.findViewById(R.id.brand);
        address= (EditText) view.findViewById(R.id.address);
        city= (EditText) view.findViewById(R.id.city);
        district= (EditText) view.findViewById(R.id.district);
        dob= (EditText) view.findViewById(R.id.dob);
        gender=(Spinner)view.findViewById(R.id.gender);
        dob.setOnClickListener(this);

        view.findViewById(R.id.save).setOnClickListener(this);

        try {
            name.setText(accountDetails.getString("name"));
            email.setText(accountDetails.getString("email"));
            if(accountDetails.getString("phone")!="null")
                phone.setText(accountDetails.getString("phone"));
            if(accountDetails.getString("dob")!="null")
                dob.setText(accountDetails.getString("dob"));
            if(accountDetails.getString("company_name")!="null")
                brand.setText(accountDetails.getString("company_name"));
            if(accountDetails.getString("address1")!="null")
                address.setText(accountDetails.getString("address1"));

            if(accountDetails.getString("city")!="null")
                city.setText(accountDetails.getString("city"));
            if(accountDetails.getString("address2")!="null")
                district.setText(accountDetails.getString("address2"));
            String g=accountDetails.getString("gender").toLowerCase();
            gender.setSelection(g.startsWith("f")?1:0);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.dob:
                showDatePicker();
            break;
            case R.id.save:
                String data="";
                try {
                    data+="name="+URLEncoder.encode(name.getText().toString(),"utf-8");;
                    data+="&email="+ URLEncoder.encode(email.getText().toString(),"utf-8");
                    data+="&phone="+ URLEncoder.encode(phone.getText().toString(),"utf-8");
                    data+="&company_name="+ URLEncoder.encode(brand.getText().toString(),"utf-8");
                    data+="&address1="+ URLEncoder.encode(address.getText().toString(),"utf-8");
                    data+="&city="+ URLEncoder.encode(city.getText().toString(),"utf-8");
                    data+="&address2="+ URLEncoder.encode(district.getText().toString(),"utf-8");
                    data+="&dob="+ URLEncoder.encode(dob.getText().toString(),"utf-8");
                    data+="&gender="+ ((gender.getSelectedItemPosition()==0)?"Male":"Female");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                new SaveMyAccount((MainActivity) getActivity()).execute(data);
                break;
        }

    }

    public Fragment with(JSONObject accountDetails) {
        this.accountDetails=accountDetails;
        return this;
    }



    private void showDatePicker() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog datePicker = new DatePickerDialog(getContext(),
                R.style.AppTheme,
                this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        dob.setText(i+"/"+(i1+1)+"/"+i2);
    }

    @Override
    public void goBack() {
        new com.evlop.kostoom.async.MyAccount((MainActivity) getActivity()).execute();
    }

    @Override
    public void backToHome() {

    }
}
