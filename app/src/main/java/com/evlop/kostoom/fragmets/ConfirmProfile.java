package com.evlop.kostoom.fragmets;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.async.CheckOut;
import com.evlop.kostoom.functions.HttpTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rachindrapoudel on 1/27/17.
 */
public class ConfirmProfile extends android.support.v4.app.Fragment {
    EditText nameEdit,phoneEdit,emailEdit,brandEdit,editAddress,editCity,editDistrict;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Profile");
        view=inflater.inflate(R.layout.checkout_confirm,null);
        nameEdit= (EditText) view.findViewById(R.id.name);
        phoneEdit= (EditText) view.findViewById(R.id.phone);
        emailEdit= (EditText) view.findViewById(R.id.email);
        brandEdit= (EditText) view.findViewById(R.id.brand);
        editAddress= (EditText) view.findViewById(R.id.address);
        editCity= (EditText) view.findViewById(R.id.city);
        editDistrict= (EditText) view.findViewById(R.id.district);

        new FetchUserData().execute();

        return view;
    }

    public boolean isValid() {


        for (String s:getDetails()) {
            if (s.trim().isEmpty()) {
                Toast.makeText(getContext(), "Please fill all the fields", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;

    }

    public String[] getDetails() {
        return new String[]{
                nameEdit.getText().toString(),
                phoneEdit.getText().toString(),
                emailEdit.getText().toString(),
                editAddress.getText().toString(),
                editCity.getText().toString(),
                editDistrict.getText().toString(),
                brandEdit.getText().toString()
        };
    }


    private class FetchUserData extends AsyncTask<String, Void, String> {
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(getActivity(), "Processing", "Please Wait...", true);
        }

        @Override
        protected String doInBackground(String... strings) {
            String source= HttpTask.baseURL+"/user/details?token="+ MainActivity.applicationData(getContext()).getString("user-token","");
            return HttpTask.get(source);
        }

        @Override
        protected void onPostExecute(String s) {
            progress.dismiss();

            try {
                JSONObject details=new JSONObject(s);
                showConfirmation(details.getString("name"),details.getString("phone"),details.getString("email"),details.getString("address1"),details.getString("city"),details.getString("address2"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private void showConfirmation(String name,String phone,String email,String address,String city,String district) {
            if(name!="null")
                nameEdit.setText(name);
            if(phone!="null")
                phoneEdit.setText(phone);
            if(email!="null")
                emailEdit.setText(email);
            if(address!="null")
                editAddress.setText(address);
            if(city!="null")
                editCity.setText(city);
            if(district!="null")
                editDistrict.setText(district);

        }
    }
}
