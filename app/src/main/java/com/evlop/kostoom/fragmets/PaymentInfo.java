package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.evlop.kostoom.R;
import com.evlop.kostoom.functions.Util;

/**
 * Created by rachindrapoudel on 1/27/17.
 */
public class PaymentInfo extends android.support.v4.app.Fragment {

    private int downPayment=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.payment_info,null);
        if(downPayment>0)
            ((TextView)view.findViewById(R.id.down_payment)).setText("Down Payment RP "+ Util.cf.format(downPayment));
        getActivity().setTitle("Payment Info");
        return view;
    }

    public void setDownPayment(int downPayment) {
        this.downPayment = downPayment;
    }
}
