package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.interfaces.GoBack;

/**
 * Created by rachindrapoudel on 2/9/17.
 */
public class ChangePassword extends Fragment implements GoBack {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.change_password);

        View view=inflater.inflate(R.layout.change_password,null);

        return view;
    }

    @Override
    public void goBack() {
        new com.evlop.kostoom.async.MyAccount((MainActivity) getActivity()).execute();
    }

    @Override
    public void backToHome() {

    }
}
