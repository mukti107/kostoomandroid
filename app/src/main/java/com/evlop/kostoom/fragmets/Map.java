package com.evlop.kostoom.fragmets;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;


public class Map extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerDragListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, PlaceSelectionListener {

    private static final String TAG = "trial-map";
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private Marker source,destination,office;
    private GoogleApiClient mGoogleApiClient;
    public static LatLng headOffice=new LatLng(-6.3927981,106.8257118);
    private LatLng fallbackSource = new LatLng(-6.3927981,106.8257118);
    private LatLng fallbackDestination = new LatLng(-6.3927981,106.8257118);
    private CameraPosition INIT;
    private FragmentActivity context;
    private String label;
    private int type;
    private SupportPlaceAutocompleteFragment autocompleteSource,autocompleteDestination;
    private View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.map, container, false);

        getActivity().setTitle("Address");

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this.context, this)
                    .build();


        }
        if (!mGoogleApiClient.isConnected()) mGoogleApiClient.connect();
        FragmentManager fm = getChildFragmentManager();
        autocompleteSource = (SupportPlaceAutocompleteFragment) fm.findFragmentById(R.id.autocomplete_source);
//        autocompleteDestination = (SupportPlaceAutocompleteFragment) fm.findFragmentById(R.id.autocomplete_destination);
        if (autocompleteSource == null ) {
            autocompleteSource = new SupportPlaceAutocompleteFragment();
            fm.beginTransaction().replace(R.id.autocomplete_source, autocompleteSource).commit();
//            autocompleteDestination = new SupportPlaceAutocompleteFragment();
//            fm.beginTransaction().replace(R.id.autocomplete_destination, autocompleteSource).commit();
        }

        autocompleteSource.setHint("Pickup location");
//        autocompleteDestination.setHint("Delivery location");
        autocompleteSource.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.i(TAG, "Place Selected: " + place.getName());
                source.setPosition(place.getLatLng());
                source.setVisible(true);
                INIT = new CameraPosition.Builder()
                        .target(place.getLatLng())
                        .zoom(17.5F)
                        .bearing(300F) // orientation
                        .tilt( 50F) // viewing angle
                        .build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
            }

            @Override
            public void onError(Status status) {
                Log.e(TAG, "onError: Status = " + status.toString());
            }
        });
 /*       autocompleteDestination.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.i(TAG, "Place Selected: " + place.getName());
                destination.setPosition(place.getLatLng());
                destination.setVisible(true);
                INIT = new CameraPosition.Builder()
                        .target(place.getLatLng())
                        .zoom(17.5F)
                        .bearing(300F) // orientation
                        .tilt( 50F) // viewing angle
                        .build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
            }

            @Override
            public void onError(Status status) {
                Log.e(TAG, "onError: Status = " + status.toString());
            }
        });
*/
        //((SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        return rootView;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(context);
        switch (result) {
            case ConnectionResult.SUCCESS:
                FragmentManager fm = getChildFragmentManager();
                mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
                if (mapFragment == null) {
                    mapFragment = SupportMapFragment.newInstance();
                    fm.beginTransaction().replace(R.id.map, mapFragment).commit();
                }
                break;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(getActivity(), "SERVICE MISSING", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(getActivity(), "UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                break;
            default:

        }


    }

    @Override
    public void onResume() {
        super.onResume();
        if (map == null) {
            mapFragment.getMapAsync(this);

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(context);
            Log.i(TAG, "location services:" + mGoogleApiClient.hasConnectedApi(LocationServices.API));
            mGoogleApiClient.disconnect();
            Log.i(TAG, "location services:" + mGoogleApiClient.hasConnectedApi(LocationServices.API));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (FragmentActivity) context;
    }


    @Override
    public void onMarkerDrag(Marker arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMarkerDragEnd(Marker arg0) {
        // TODO Auto-generated method stub
        LatLng dragPosition = arg0.getPosition();
        double dragLat = dragPosition.latitude;
        double dragLong = dragPosition.longitude;

    }

    @Override
    public void onMarkerDragStart(Marker arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMapClick(LatLng arg0) {
        // TODO Auto-generated method stub
        map.animateCamera(CameraUpdateFactory.newLatLng(arg0));
    }


    @Override
    public void onMapLongClick(LatLng arg0) {
//        marker.setPosition(arg0);
//        if (!marker.isVisible()) marker.setVisible(true);
    }


    @Override
    public void onConnected(Bundle bundle) {


    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMyLocationButtonClick() {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);

            return false;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            source.setPosition(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
            source.setVisible(true);
            INIT =
                    new CameraPosition.Builder()
                            .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                            .zoom(17.5F)
                            .bearing(300F) // orientation
                            .tilt( 50F) // viewing angle
                            .build();

            map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
            Log.i(Map.TAG,"marker market set to last known position");
        }
        return true;
    }


    @Override
    public void onClick(View v) {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);
        }else
            googleMap.setMyLocationEnabled(true);
        googleMap.setOnMarkerDragListener(this);
        googleMap.setOnMyLocationButtonClickListener(this);
        googleMap.setOnMapLongClickListener(this);
        googleMap.setOnMapClickListener(this);
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        source = googleMap.addMarker(new MarkerOptions()
                .position(fallbackSource)
                .title("Pickup")
                .visible(true)
                .draggable(true));
        destination = googleMap.addMarker(new MarkerOptions()
                .position(fallbackDestination)
                .title("Delivery")
                .visible(false)
                .draggable(true));
        office = googleMap.addMarker(new MarkerOptions()
                .position(headOffice)
                .title("Kostoom")
                .snippet("Head Office")
                .draggable(false));
        office.showInfoWindow();

        Log.i(TAG, "map and marker settings set");


            source.setPosition(fallbackSource);
            INIT =
                    new CameraPosition.Builder()
                            .target(fallbackSource)
                            .zoom(17.5F)
                            .bearing(300F) // orientation
                            .tilt(50F) // viewing angle
                            .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

        map=googleMap;

        if(source.getPosition().latitude==office.getPosition().latitude && source.getPosition().longitude==office.getPosition().longitude)
            onMyLocationButtonClick();

    }

    @Override
    public void onPlaceSelected(Place place) {
        Log.i(TAG, "Place Selected: " + place.getName());
        source.setPosition(place.getLatLng());
        source.setVisible(true);
        INIT = new CameraPosition.Builder()
                        .target(source.getPosition())
                        .zoom(17.5F)
                        .bearing(300F) // orientation
                        .tilt( 50F) // viewing angle
                        .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
    }

    @Override
    public void onError(Status status) {
        Log.e(TAG, "onError: Status = " + status.toString());
    }


    public String getPickupLocation() {
        return source.getPosition().latitude+","+source.getPosition().longitude;
    }

    public String getDeliveryLocation() {
        return destination.getPosition().latitude+","+destination.getPosition().longitude;
    }

    public Map withLocation(String pickupLocation, String deliveryLocation) {
        String[] pLatLong=pickupLocation.split(",");
        String[] dLatLong=deliveryLocation.split(",");
        if(pLatLong.length>1){
            fallbackSource=new LatLng(Double.parseDouble(pLatLong[0]),Double.parseDouble(pLatLong[1]));
        }

        if(dLatLong.length>1){
            fallbackDestination=new LatLng(Double.parseDouble(dLatLong[0]),Double.parseDouble(dLatLong[1]));
        }

        return this;
    }
}