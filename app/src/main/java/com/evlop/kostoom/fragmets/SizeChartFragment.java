package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.evlop.kostoom.R;
import com.evlop.kostoom.model.SizeChart;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.Map;

public class SizeChartFragment extends Fragment{

    private JSONObject sizeChartAll;
    private String label;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.size_chart_table,null);


        TableLayout table= (TableLayout) view.findViewById(R.id.size_chart_container);

        Iterator<String> keys = sizeChartAll.keys();
        while(keys.hasNext()){
            String key=keys.next();
            TableRow row= (TableRow) getActivity().getLayoutInflater().inflate(R.layout.size_chart_record,null);
            try {
                ((TextView)row.findViewById(R.id.size_chart_label)).setText(key);
                EditText value=(EditText)row.findViewById(R.id.size_chart_input);
                value.setText(""+sizeChartAll.getJSONObject(key).getInt(label));
                value.addTextChangedListener(new MyTextWatcher(key,label));
                value.setId((int) (Math.random()*100000000));
                table.addView(row);
                Log.i("sizechart : "+key,sizeChartAll.getJSONObject(key).get(label)+"");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.i("sizechart",sizeChartAll.toString());


        return view;
    }



    public SizeChartFragment with(JSONObject sizeCharts, String label) {
        this.sizeChartAll=sizeCharts;
        this.label=label;
        return this;
    }


    private class MyTextWatcher implements TextWatcher{

        private final String key;
        private final String label;

        public MyTextWatcher(String key, String label) {
            this.key=key;
            this.label=label;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            int resp= Integer.parseInt("0"+String.valueOf(editable));
            try {
                sizeChartAll.getJSONObject(key).put(label,resp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
