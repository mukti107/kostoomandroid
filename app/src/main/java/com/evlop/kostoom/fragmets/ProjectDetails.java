package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.adapters.ImageDisplayAdapter;
import com.evlop.kostoom.adapters.UrlImageDisplayAdapter;
import com.evlop.kostoom.async.IntrestOnOrder;
import com.evlop.kostoom.functions.Util;
import com.evlop.kostoom.interfaces.GoBack;
import com.evlop.kostoom.model.FabricMatch;

import org.json.JSONException;
import org.json.JSONObject;

import static com.evlop.kostoom.MainActivity.context;
import static com.evlop.kostoom.fragmets.BusinessSewing.buttonsValue;
import static com.evlop.kostoom.fragmets.BusinessSewing.zipperValue;

/**
 * Created by rh on 12/10/16.
 */
public class ProjectDetails extends Fragment implements View.OnClickListener,GoBack {

    JSONObject order=null;
    private Fragment returnFragment=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.details);
        View view=inflater.inflate(R.layout.order_details,null);
        String[] images=new String[0];

        Log.i("data",order.toString());
        try{
            ((TextView)view.findViewById(R.id.item_name)).setText(order.getString("item_name"));
            ((TextView)view.findViewById(R.id.item_quantity)).setText(""+order.getString("quantity"));
            images=new String[order.getJSONArray("images").length()];
            for (int i=0;i<images.length;i++)
                images[i]=order.getJSONArray("images").getString(i);
//            ((TextView)view.findViewById(R.id.accessories_zipper)).setText(zipperValue[order.getZipper()]);
//            ((TextView)view.findViewById(R.id.accessories_button)).setText(buttonsValue[order.getButton()]);
        } catch (JSONException e) {
            e.printStackTrace();
        }



        GridView designImages=(GridView)view.findViewById(R.id.design_images);
        designImages.setAdapter(new UrlImageDisplayAdapter(context,images));


//        LinearLayout fabricMatch=(LinearLayout)view.findViewById(R.id.fabric_match);
//        for(FabricMatch match: order.getFabricMatch()){
//            TextView tv=new TextView(context);
//            tv.setText(context.getResources().getStringArray(R.array.size_list)[match.getType()]+" : "+match.getQuantity());
//            fabricMatch.addView(tv);
//        }
//        ((TextView)view.findViewById(R.id.price)).setText(" RP. "+ Util.cf.format(order.getPrice(order.getQuantity())));



//        TextView name=(TextView)view.findViewById(R.id.name);
//        TextView status=(TextView)view.findViewById(R.id.status);
//        TextView quantity=(TextView)view.findViewById(R.id.quantity);
//        TextView description=(TextView)view.findViewById(R.id.description);
//
//        try {
//            name.setText(order.getString("item_name"));
//            quantity.setText(order.getString("quantity")+" pieces");
//            status.setText("Status: "+order.getString("status"));
//            description.setText(order.getString("item_description"));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        Button intrest=(Button)view.findViewById(R.id.intrest);

        try {
            intrest.setText((order.getBoolean("intrested")?R.string.not_intrested:R.string.intrested));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(returnFragment instanceof MyOffers || returnFragment instanceof MyOrders)
            intrest.setVisibility(View.GONE);

        intrest.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.intrest:
                try {
                    new IntrestOnOrder((MainActivity)getActivity(),order.getString("id"),!order.getBoolean("intrested")).execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    public ProjectDetails withOrder(JSONObject order) {
        this.order=order;
        return this;
    }

    @Override
    public void goBack() {
        ((MainActivity)getActivity()).openFargment(returnFragment);
    }

    @Override
    public void backToHome() {
        ((MainActivity)getActivity()).openHome();
    }

    public ProjectDetails fromFragment(Fragment returnFragment) {

        this.returnFragment=returnFragment;
        return this;
    }
}
