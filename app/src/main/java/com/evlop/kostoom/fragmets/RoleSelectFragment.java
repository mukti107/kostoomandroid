package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.evlop.kostoom.R;
import com.evlop.kostoom.Role;
import com.evlop.kostoom.model.SizeChart;

/**
 * Created by rachindrapoudel on 12/16/16.
 */
public class RoleSelectFragment extends Fragment{


    String title;
    private View.OnClickListener listner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.role_select,null);

        Button confirm=(Button)view.findViewById(R.id.confirm);
        LinearLayout img= (LinearLayout) view.findViewById(R.id.role_bg);

        switch (title.toLowerCase()){
            case Role.CUSTOMER:
                img.setBackgroundResource(R.mipmap.role_customer);
                break;
            case Role.TAILOR:
                img.setBackgroundResource(R.mipmap.role_tailor);
        }


        confirm.setText("Continue as "+title);
        confirm.setTag(title);
        confirm.setOnClickListener(listner);
        return view;
    }

    public RoleSelectFragment with(CharSequence title) {
        this.title= (String) title;
        return this;
    }

    public RoleSelectFragment setListner(View.OnClickListener listner) {
        this.listner= listner;
        return this;
    }
}
