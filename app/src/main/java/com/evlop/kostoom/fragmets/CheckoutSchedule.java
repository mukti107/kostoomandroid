package com.evlop.kostoom.fragmets;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.evlop.kostoom.R;
import com.evlop.kostoom.functions.HttpTask;
import com.evlop.kostoom.model.*;
import com.evlop.kostoom.model.Checkout;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static android.R.attr.data;

/**
 * Created by rachindrapoudel on 1/26/17.
 */
public class CheckoutSchedule extends android.support.v4.app.Fragment implements DatePickerDialog.OnDateSetListener, View.OnClickListener {
    View view;
    private Checkout data;
    String locationString;
    private com.evlop.kostoom.fragmets.Checkout checkout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Schedule");
        view=inflater.inflate(R.layout.checkout_schedule,null);

        view.findViewById(R.id.date).setOnClickListener(this);
        new ValidateCoverage().execute();
        return view;
    }

    private void showDatePicker() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog datePicker = new DatePickerDialog(getContext(),
                R.style.AppTheme,
                this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }

    private void showTimePicker(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        //TimePickerDialog mTimePicker;
        //mTimePicker = new TimePickerDialog(getContext(), this, hour, minute, false);//Yes 24 hour time
        //mTimePicker.setTitle("Select Time");
        //mTimePicker.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar cal=Calendar.getInstance();
        cal.set(i,i1,i2);
        int day= cal.get(Calendar.DAY_OF_WEEK);
        long diff=cal.getTimeInMillis()-System.currentTimeMillis();

        long days=TimeUnit.MILLISECONDS.toDays(diff);
        if(days>15 || days<0){
            Toast.makeText(getContext(),"Please select date within 14 days starting from tommorow.",Toast.LENGTH_LONG).show();
            return;
        }

        if(Calendar.getInstance().get(Calendar.DAY_OF_MONTH)==cal.get(Calendar.DAY_OF_MONTH)){
            Toast.makeText(getContext(),"Please select a date other than today",Toast.LENGTH_LONG).show();
            return;
        }

        if(day==1 || day==7){
            Toast.makeText(getContext(),"Please select date other than Saturday, Sunday.",Toast.LENGTH_LONG).show();
            return;
        }
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        ((EditText)view.findViewById(R.id.date)).setText(i2+" "+monthNames[i1].substring(0,3)+" "+i);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.date:
                showDatePicker();
                break;

            case R.id.time:
                showTimePicker();
                break;
        }
    }

    public String getDate() {
        return ((EditText)view.findViewById(R.id.date)).getText().toString();
    }

    public String getTime() {
        return ((Spinner)view.findViewById(R.id.time)).getSelectedItem().toString();
    }

    public String getVehicle() {
        RadioGroup rg= (RadioGroup) view.findViewById(R.id.vehicles);
        RadioButton selected= (RadioButton) view.findViewById(rg.getCheckedRadioButtonId());
        return selected.getText().toString();
    }

    public CheckoutSchedule withData(Checkout data) {
        this.data=data;
        return this;
    }

    public CheckoutSchedule withParent(com.evlop.kostoom.fragmets.Checkout checkout) {
        this.checkout=checkout;
        return this;
    }


    private class ValidateCoverage extends AsyncTask<String, Void, Integer> {
        @Override
        protected Integer doInBackground(String... strings) {
            final String to=data.getPickupLocation();
            final String from="-6.3927981,106.8257118";
            SyncHttpClient client = new SyncHttpClient();
            RequestParams params = new RequestParams();
            params.add("origin", from);
            params.add("destination", to);
            params.add("key", "AIzaSyDBaL8X3LlEh0Z7zYYk0HfiAkpaE0tQiN0");
            try {

                String responseBody= HttpTask.get("https://maps.googleapis.com/maps/api/directions/json?key=AIzaSyDBaL8X3LlEh0Z7zYYk0HfiAkpaE0tQiN0&origin="+ URLEncoder.encode(from,"utf-8")+"&destination="+ URLEncoder.encode(to,"utf-8"));
                Log.i("location",responseBody);
                JSONObject result= new JSONObject(new String(responseBody));
                JSONObject routes=result.getJSONArray("routes").getJSONObject(0);
                JSONArray legs=routes.getJSONArray("legs");
                Integer distance=legs.getJSONObject(0).getJSONObject("distance").getInt("value");
                data.setDistance(""+distance);
                data.setAddress(legs.getJSONObject(0).getString("end_address"));
                locationString="<b>Pickup location</b><br/>"+legs.getJSONObject(0).getString("end_address");
                locationString+="<br/><br/><b>Distance:</b>"+legs.getJSONObject(0).getJSONObject("distance").getString("text")+"<br/>";

                return distance;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer s) {
            if(s>60000)
                view.findViewById(R.id.pickup_not_available).setVisibility(View.VISIBLE);

            com.evlop.kostoom.fragmets.Checkout.distance=s/1000;
            checkout.updateCartDetails();

            ((TextView)view.findViewById(R.id.location)).setText(Html.fromHtml(locationString));
        }
    }

}
