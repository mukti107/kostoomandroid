package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.adapters.ItemListAdapter;
import com.evlop.kostoom.async.FetcProducts;
import com.evlop.kostoom.components.ImageSlider;
import com.evlop.kostoom.model.Product;
import com.evlop.kostoom.model.SewingOrder;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by rh on 12/10/16.
 */
public class ItemList extends Fragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    static ArrayList<Product> products=null;
    SwipeRefreshLayout refresh;
    private Timer timer;
    private int currentPage=0;
    private ViewPager vpPager;
    private Handler handler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getActivity().setTitle(R.string.sewing_order);

        if(products==null) {
            new FetcProducts((MainActivity) getActivity()).execute();
            return null;
        }

        View view=inflater.inflate(R.layout.item_list,null);
        GridView items= (GridView) view.findViewById(R.id.items);
        refresh=(SwipeRefreshLayout)view.findViewById(R.id.sync);
        refresh.setOnRefreshListener(this);
        handler = new Handler();

        vpPager = (ViewPager) view.findViewById( R.id.images);
        new ImageSlider(view,new int[]{R.mipmap.banner_main_1,R.mipmap.banner_main_2},getChildFragmentManager(), R.id.images);

        items.setAdapter(new ItemListAdapter(getActivity().getApplicationContext(),products));
        items.setOnItemClickListener(this);
        timer();
        return view;
    }

    private void timer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (currentPage == 2) {
                            currentPage = 0;
                        }
                        vpPager.setCurrentItem(currentPage++);

                    }
                });
            }
        }, 500, 5000);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SewingOrder order=new SewingOrder();
        order.setItem(products.get(position));
        BusinessSewing sewingOrderFragment=new BusinessSewing();
        sewingOrderFragment.setOrder(order);
        ((MainActivity)getActivity()).openFargment(sewingOrderFragment);
    }

    public Fragment withProducts(ArrayList<Product> productList) {
        this.products=productList;
        return this;
    }

    @Override
    public void onRefresh() {
        new FetcProducts((MainActivity) getActivity()).execute();
        refresh.setRefreshing(false);

    }
}
