package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evlop.kostoom.R;

/**
 * Created by rh on 12/10/16.
 */
public class Invoice extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.invoice,null);

        return view;
    }

    @Override
    public void onClick(View v) {

    }
}
