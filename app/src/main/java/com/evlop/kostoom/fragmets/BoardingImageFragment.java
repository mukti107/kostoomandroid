package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evlop.kostoom.ImageSwipe;
import com.evlop.kostoom.R;

/**
 * Created by rh on 12/8/16.
 */
public class BoardingImageFragment extends Fragment {

    int image;
    private ImageSwipe.BoardingImage boardingImage;

    public BoardingImageFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View imageView=inflater.inflate(R.layout.boarding_image,null);
        ((ImageView)imageView.findViewById(R.id.image)).setImageResource(boardingImage.getImage());
        ((TextView)imageView.findViewById(R.id.title)).setText(boardingImage.getTitle());
        ((TextView)imageView.findViewById(R.id.description)).setText(boardingImage.getDescription());
        return imageView;
    }

    public BoardingImageFragment withBoardingImage(ImageSwipe.BoardingImage boardingImage) {
        this.boardingImage=boardingImage;
        return this;
    }
}

