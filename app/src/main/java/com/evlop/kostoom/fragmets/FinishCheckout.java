package com.evlop.kostoom.fragmets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evlop.kostoom.R;

/**
 * Created by rachindrapoudel on 1/27/17.
 */
public class FinishCheckout extends android.support.v4.app.Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.checkout_success,null);

    }
}
