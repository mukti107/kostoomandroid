package com.evlop.kostoom.fragmets;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.evlop.kostoom.Constants;
import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.adapters.ImageDisplayAdapter;
import com.evlop.kostoom.adapters.ImageGrabber;
import com.evlop.kostoom.adapters.ImageUploadAdapter;
import com.evlop.kostoom.adapters.SizeChartAdapter;
import com.evlop.kostoom.async.FetcProducts;
import com.evlop.kostoom.components.AlertDialogActions;
import com.evlop.kostoom.components.ImageSlider;
import com.evlop.kostoom.components.KostoomAlertDialog;
import com.evlop.kostoom.functions.Util;
import com.evlop.kostoom.interfaces.GoBack;
import com.evlop.kostoom.model.FabricMatch;
import com.evlop.kostoom.model.Image;
import com.evlop.kostoom.model.SewingOrder;
import com.evlop.kostoom.model.SizeChart;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by rh on 12/11/16.
 */
public class BusinessSewing extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener,GoBack {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 78;
    private static int totalItems=0;
    private static int totalOrders=0;
    SewingOrder order;
    int step=0;
    public GridView gridView;
    public ArrayList<Image> imageReceiver;
    private PagerAdapter sizeChartAdapter=null;
    CheckBox sizeChartConfirm,accessoriesConfirm;

    public static int[] buttonsValue=new int[]{R.string.shirt_button,R.string.wrap_button,R.string.provide_button,R.string.no_button_required,R.string.others};
    public static int[] zipperValue=new int[]{R.string.regular_zipper,R.string.japan_zipper,R.string.provide_zipper,R.string.no_zipper_required,R.string.others};
    public static int[] furValue=new int[]{R.string.cb_no_furing,R.string.cb_furing_self,R.string.cb_furing_kostoom};
    View view=null;
    private Timer timer;
    Handler handler;
    boolean timerRunning=false;
    private int currentPage=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(view!=null)
            for (int v:new int[]{R.id.size_chart,R.id.upload_design_images,R.id.quantity_images}){
                ViewPager vp=( ViewPager)view.findViewById(v);
                if(vp!=null){
                    vp.removeAllViews();
                    vp.setAdapter(null);
                    vp.invalidate();
                }
            }

        handler=new Handler();
        switch (step){
            case 0:
                getActivity().setTitle(R.string.quantity);
                view=inflater.inflate(R.layout.product_details,null);
                new ImageSlider(view,new int[]{R.mipmap.banner_quantity_1,R.mipmap.banner_quantity_2},getChildFragmentManager(), R.id.quantity_images);
                TextView itemName=(TextView)view.findViewById(R.id.name);
                final Button uploadImage=(Button)view.findViewById(R.id.next);
                Button priceList=(Button)view.findViewById(R.id.price_list);
                Button condition=(Button)view.findViewById(R.id.condition);
                EditText quantity=(EditText)view.findViewById(R.id.quantity);
                if(order.getQuantity()>0)
                    quantity.setText(""+order.getQuantity());

                quantity.addTextChangedListener(new TextWatcher() {
                    @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                    @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                    @Override
                    public void afterTextChanged(Editable editable) {
                        int quantity=Integer.parseInt("0"+String.valueOf(editable));
                        order.setQuantity(quantity);
                        validateSteps();
                    }
                });
                uploadImage.setOnClickListener(this);
                priceList.setOnClickListener(this);
                condition.setOnClickListener(this);
                itemName.setText(order.getItem().getName());
            break;
            case 1:
                getActivity().setTitle(R.string.upload_design);
                view=inflater.inflate(R.layout.upload_design,null);
                new ImageSlider(view,new int[]{R.mipmap.banner_upload},getChildFragmentManager(),R.id.upload_design_images);
                Button done=(Button)view.findViewById(R.id.next);
                done.setOnClickListener(this);

                ((Spinner)view.findViewById(R.id.has_fabric)).setSelection(order.isFabricWithCustomer()?1:0);
                ((Spinner)view.findViewById(R.id.has_fabric)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                           order.setFabricWithCustomer(i==1);
                    }
                    @Override public void onNothingSelected(AdapterView<?> adapterView) {}
                });
                ((EditText)view.findViewById(R.id.design_note)).setText(order.getDesignNotes());
                ((EditText)view.findViewById(R.id.design_note)).addTextChangedListener(new TextWatcher() {
                    @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                    @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                    @Override
                    public void afterTextChanged(Editable editable) {
                        order.setDesignNotes(String.valueOf(editable));
                    }
                });

                gridView=(GridView)view.findViewById(R.id.design_images);
                gridView.setAdapter(new ImageUploadAdapter(getActivity(),order.getImages()));
                gridView.setOnItemClickListener(this);
               break;
            case 2:
                getActivity().setTitle(R.string.fabric_variation);
                view=inflater.inflate(R.layout.mix_match,null);

                (view.findViewById(R.id.add_variation)).setOnClickListener(this);

                Button confirmMixMatch=(Button) view.findViewById(R.id.next);
                confirmMixMatch.setOnClickListener(this);
                if(order.getFabricMatch().size()==0)
                    order.getFabricMatch().add(new FabricMatch());
                LinearLayout mixMatch= (LinearLayout) view.findViewById(R.id.mix_match_container);
                populateMixMatch(mixMatch,order.getFabricMatch(),true);
                break;
            case 3:
                getActivity().setTitle(R.string.size_chart);
                view=inflater.inflate(R.layout.size_chart,null);
                Button sizeChartDone=(Button) view.findViewById(R.id.next);
                ViewPager sizeChart=( ViewPager)view.findViewById(R.id.size_chart);
                sizeChartConfirm=(CheckBox)view.findViewById(R.id.size_chart_confirm);
                sizeChartConfirm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        validateSteps();
                    }
                });
                if (sizeChartAdapter==null)
                    sizeChartAdapter=new SizeChartAdapter(getChildFragmentManager(),getContext()).with(order);
                sizeChart.setAdapter(sizeChartAdapter);
                sizeChartDone.setOnClickListener(this);
                break;
            case 4:
                getActivity().setTitle(R.string.accessories);
                view=inflater.inflate(R.layout.accessories,null);

                Button accessoriesDone=(Button) view.findViewById(R.id.next);
                accessoriesConfirm=(CheckBox)view.findViewById(R.id.accessories_confirm);
                accessoriesConfirm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        validateSteps();
                    }
                });

                RadioButton[] buttons=new RadioButton[]{
                        (RadioButton)view.findViewById(R.id.cb_button_regular),
                        (RadioButton)view.findViewById(R.id.cb_button_wrap),
                        (RadioButton)view.findViewById(R.id.cb_button_shell),
                        (RadioButton)view.findViewById(R.id.cb_button_no),
                        (RadioButton)view.findViewById(R.id.cb_button_others)
                } ;
                MyCheckedChangeListner buttonListner=new MyCheckedChangeListner(buttons);
                RadioButton[] zippers=new RadioButton[]{
                        (RadioButton)view.findViewById(R.id.cb_zipper_regular),
                        (RadioButton)view.findViewById(R.id.cb_zipper_japan),
                        (RadioButton)view.findViewById(R.id.cb_zipper_metal),
                        (RadioButton)view.findViewById(R.id.cb_zipper_no),
                        (RadioButton)view.findViewById(R.id.cb_zipper_others)
                } ;
                RadioButton[] furing=new RadioButton[]{
                        (RadioButton)view.findViewById(R.id.cb_no_furing),
                        (RadioButton)view.findViewById(R.id.cb_furing_self),
                        (RadioButton)view.findViewById(R.id.cb_furing_kostoom),
                } ;

                zippers[order.getZipper()].setChecked(true);
                buttons[order.getButton()].setChecked(true);
                furing[order.getFuring()].setChecked(true);

                EditText notes=(EditText)view.findViewById(R.id.accessories_note);
                notes.setText(order.getAccessoriesNote());

                notes.addTextChangedListener(new TextWatcher() {@Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}@Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                    @Override
                    public void afterTextChanged(Editable editable) {
                        order.setAccessoriesNote(String.valueOf(editable));
                    }
                });

                MyCheckedChangeListner zipperListner=new MyCheckedChangeListner(zippers);
                MyCheckedChangeListner furingListner=new MyCheckedChangeListner(furing);
                for (RadioButton button:buttons){button.setOnCheckedChangeListener(buttonListner);button.setTag("button");}
                for (RadioButton zipper:zippers){zipper.setOnCheckedChangeListener(zipperListner); zipper.setTag("zipper");}
                for (RadioButton fur:furing){fur.setOnCheckedChangeListener(furingListner); fur.setTag("furing");}
                accessoriesDone.setOnClickListener(this);
                break;

            case 5:
                getActivity().setTitle(R.string.order_summary);
                view=inflater.inflate(R.layout.order_summary,null);
                generateSummary(view,order,getContext());
                view.findViewById(R.id.add_to_cart).setOnClickListener(this);
                break;

            default:
                view=super.onCreateView(inflater, container, savedInstanceState);
        }

        int[] steps=new int[]{R.id.step0,R.id.step1,R.id.step2,R.id.step3,R.id.step4,R.id.step5};
            for(int i=0;i<steps.length;i++){
                ImageView iv=(ImageView)view.findViewById(steps[i]);
                if(step==i)
                    iv.setAlpha(1f);
                else
                    iv.setAlpha(0.5f);

            }
        validateSteps();
        if(!timerRunning)
            timer();

        return view;
    }

    private void timer() {
        timerRunning=true;
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.i("timer","running");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (currentPage == 2) {
                            currentPage = 0;
                        }
                        Log.i("timer","running inside");
                        ViewPager vp=null;
                        if(step==0)
                            vp= (ViewPager) view.findViewById(R.id.quantity_images);
//                        else if(step==1)
//                            vp= (ViewPager) view.findViewById(R.id.upload_design_images);
                        if(vp!=null)
                        vp.setCurrentItem(currentPage++);

                    }
                });
            }
        }, 500, 5000);
    }

    public static void generateSummary(View view, SewingOrder order,Context context) {
        ((TextView)view.findViewById(R.id.item_name)).setText(order.getItem().getName());
        ((TextView)view.findViewById(R.id.design_notes)).setText(order.getDesignNotes());
        ((TextView)view.findViewById(R.id.item_quantity)).setText(""+order.getQuantity());
        ((TextView)view.findViewById(R.id.accessories_zipper)).setText(zipperValue[order.getZipper()]);
        ((TextView)view.findViewById(R.id.accessories_button)).setText(buttonsValue[order.getButton()]);
        ((TextView)view.findViewById(R.id.accessories_furing)).setText(furValue[order.getFuring()]);

        GridView designImages=(GridView)view.findViewById(R.id.design_images);
        designImages.setAdapter(new ImageDisplayAdapter(context,order.getImages()));


        LinearLayout fabricMatch=(LinearLayout)view.findViewById(R.id.fabric_match);
        for(FabricMatch match: order.getFabricMatch()){

            LinearLayout ll=new LinearLayout(context);
            ll.setOrientation(LinearLayout.VERTICAL);
            TextView tv=new TextView(context);
            TextView notes=new TextView(context);
            tv.setText(context.getResources().getStringArray(R.array.size_list)[match.getType()]+" : "+match.getQuantity());
            notes.setText(match.getNote());
            ll.addView(tv);
            ll.addView(notes);
            fabricMatch.addView(ll);
        }
        ((TextView)view.findViewById(R.id.furing_price)).setText(" RP. "+ Util.cf.format(order.getFuringPriceWithTransactionDiscount()));
        ((TextView)view.findViewById(R.id.price)).setText(" RP. "+ Util.cf.format(order.getNetPriceWithTransactionDiscount()+order.getFuringPriceWithTransactionDiscount()));
    }

    private void populateMixMatch(LinearLayout mixMatch, ArrayList<FabricMatch> mixAndMatch,boolean clear) {
        if(clear)
            mixMatch.removeAllViews();
        for ( final FabricMatch match:mixAndMatch){
            LayoutInflater inflater=getLayoutInflater(null);
            final View v=inflater.inflate(R.layout.add_mix_match_record,null);
            GridView gridView=(GridView) v.findViewById(R.id.upload_fabric);

            if(order.getFabricMatch().indexOf(match)==0)
                v.findViewById(R.id.close).setVisibility(View.GONE);

            v.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    order.getFabricMatch().remove(match);
                    v.setVisibility(View.GONE);
                    validateSteps();
                }
            });

            EditText quantity= (EditText) v.findViewById(R.id.quantity);
            EditText note= (EditText) v.findViewById(R.id.note);
            note.setText(match.getNote());
            note.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void afterTextChanged(Editable editable) {
                    match.setNote(String.valueOf(editable));
                }
            });
            if(match.getQuantity()>0)
                quantity.setText(""+match.getQuantity());
            quantity.requestFocus();
            quantity.addTextChangedListener(new TextWatcher() {
                @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void afterTextChanged(Editable editable) {
                    Log.i("editable",editable+"");
                    int quantity=Integer.parseInt("0"+String.valueOf(editable));
                    match.setQuantity(quantity);
                    validateSteps();
                }
            });

            final Spinner size= (Spinner) v.findViewById(R.id.size_select);
            size.setSelection(match.getType());
            size.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    match.setType(size.getSelectedItemPosition());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            ArrayList<Image> images = match.getImages();
            gridView.setAdapter(new ImageUploadAdapter(getContext(),images));
            gridView.setOnItemClickListener(new ImageGrabber(this,images,gridView));
            mixMatch.addView(v);
        }
    }

    public void setOrder(SewingOrder order) {
        this.order = order;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.price_list:
                showPriceList();
                break;
            case R.id.condition:
                showCondition();
                break;
            case R.id.next:
                switch (step) {
                    case 0:
                        if (order.getQuantity() > 0)
                            step = 1;
                        else {
                            Toast.makeText(getContext(), R.string.warning_order_quantity, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        break;
                    case 1:
                        if (order.getImages().size() > 0)
                            step = 2;
                        else {
                            Toast.makeText(getContext(), R.string.warning_upload_image, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        break;
                    case 2:
                        ArrayList<Integer> sizesUsed = new ArrayList<>();
                        for (FabricMatch match : order.getFabricMatch()) {

                            if (sizesUsed.contains(match.getType())) {
                                Toast.makeText(getContext(), "There cannot be multiple variation for same size.", Toast.LENGTH_LONG).show();
                                return;
                            } else {
                                sizesUsed.add(match.getType());
                            }

                            if (match.getQuantity() == 0) {
                                Toast.makeText(getContext(), "One or more of Fabric variation have quantity 0, Please make sure each variation have some quantity.", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                        if (order.isMixMatchValid())
                            step = 3;
                        else {

                            return;
                        }
                        break;
                    case 3:
                        if (!sizeChartConfirm.isChecked()) {
                            Toast.makeText(getContext(), R.string.warning_confirm_size_chart, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        step = 4;
                        break;
                    case 4:
                        if (!accessoriesConfirm.isChecked()) {
                            Toast.makeText(getContext(), R.string.warning_confirm_accessories, Toast.LENGTH_SHORT).show();
                            return;
                        } else
                            step = 5;
                        break;
                }
                break;
            case R.id.add_to_cart:
                confirmAddToCart();
                break;
            case R.id.add_variation:
                FabricMatch fm = new FabricMatch();
                order.getFabricMatch().add(fm);
                ArrayList<FabricMatch> al = new ArrayList<>();
                al.add(fm);
                populateMixMatch((LinearLayout) view.findViewById(R.id.mix_match_container), al, false);
                return;
        }
        getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

    private void showPriceList() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.price_list, null);
        ImageView imageView=(ImageView) dialogView.findViewById(R.id.image);
        ImageLoader.getInstance().displayImage(order.getItem().getImageUrl(),imageView);
        int[] pri=new int[]{R.id.p1,R.id.p2,R.id.p3,R.id.p4,R.id.p5,R.id.p6};
        for(int i=0;i<pri.length;i++){
            ((TextView)dialogView.findViewById(pri[i])).setText("Rp "+Math.round(order.getItem().getPrice().getPriceAsArray()[i]/1000)+".000");
        }


        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialogView.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void showCondition() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.condition, null);
        ImageView imageView=(ImageView) dialogView.findViewById(R.id.image);
        ImageLoader.getInstance().displayImage(order.getItem().getImageUrl(),imageView);

        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialogView.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    @Override
    public void backToHome() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.backto_home_alert, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View.OnClickListener listner=new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if (view.getId()==R.id.yes)
                        ((MainActivity)getActivity()).openFargment(new ItemList());
            }
        };
        dialogView.findViewById(R.id.close).setOnClickListener(listner);
        dialogView.findViewById(R.id.no).setOnClickListener(listner);
        dialogView.findViewById(R.id.yes).setOnClickListener(listner);

        alertDialog.show();
    }

    private void confirmAddToCart() {
        KostoomAlertDialog dialog=new KostoomAlertDialog().setActivity(getActivity());
        dialog.setMessage("Do you want to add this model to cart?");
        dialog.setTitle("Add to cart");
        dialog.setActions(new AlertDialogActions() {
            @Override
            public void onPositiveAction() {
                addToCart();
                orderSuccess();
            }
            @Override public void onNegativeAction() {}
        });
        dialog.show();
    }

    private void orderSuccess() {

        KostoomAlertDialog dialog=new KostoomAlertDialog().setActivity(getActivity());
        dialog.setMessage("Do you want to add other model or check cart?");
        dialog.setTitle("Cart Success");
        dialog.setYesText("Add another");
        dialog.setNoText("Check Cart");
        dialog.setActions(new AlertDialogActions() {
            @Override
            public void onPositiveAction() {
                new FetcProducts((MainActivity) getActivity()).execute();
            }
            @Override public void onNegativeAction() {
                ((MainActivity)getActivity()).openFargment(new Checkout());
            }
        });
        dialog.show();
    }

    private void addToCart() {

        for(SizeChart s:order.getSizeChart())
            s.save();
        order.getItem().getPrice().save();
        order.getItem().save();
        Long orderid=order.save();

        for(Image i:order.getImages()) {
            i.setSewingOrder(orderid);
            i.save();
        }
        for(FabricMatch f:order.getFabricMatch()) {
            f.setSewingOrder(orderid);
            long matchid=f.save();
            for(Image i:f.getImages()) {
                i.setFabricMatch(matchid);
                i.save();

            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        if(position>=order.getImages().size()) {
            imageReceiver = order.getImages();
            selectImage();
        }
        else {
            KostoomAlertDialog dialog=new KostoomAlertDialog().setActivity(getActivity());
            dialog.setMessage("Are you sure you want to remove the design image?");
            dialog.setActions(new AlertDialogActions() {
                @Override
                public void onPositiveAction() {
                    order.getImages().remove(position);
                    gridView.invalidateViews();
                    validateSteps();
                }

                @Override
                public void onNegativeAction() {

                }
            });
            dialog.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode!=MainActivity.RESULT_OK)
            return;

        switch (requestCode){
            case Constants.REQUEST_CAMERA:
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imageReceiver.add(new Image(photo));
                break;

            case Constants.REQUEST_FILE:


                Uri uri = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    imageReceiver.add(new Image(bitmap));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
        validateSteps();
        gridView.invalidateViews();
    }



    public void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, Constants.REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {



                    if (Build.VERSION.SDK_INT >= 23){
                        if (ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {

                            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            } else {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                                return;
                            }
                        }
                    }
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,
                            "Select Picture"), Constants.REQUEST_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void goBack() {
        if(step>0) {
            step--;
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }else {
            backToHome();
        }
    }


    private void validateSteps(){
        Button next=(Button)view.findViewById(R.id.next);
        RelativeLayout footer=(RelativeLayout) view.findViewById(R.id.footer);
        switch (step){
            case 0:
                next.setEnabled(order.getQuantity()>0);

                ((TextView)view.findViewById(R.id.price)).setText(" Rp."+Util.cf.format((int)(order.getNetPriceWithTransactionDiscount()/order.getQuantity()))+"/Piece");

                if(order.getQuantity()>0) {
                    view.findViewById(R.id.estimated_price).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.conditions).setVisibility(View.VISIBLE);
                }
                else {
                    view.findViewById(R.id.estimated_price).setVisibility(View.GONE);
                    view.findViewById(R.id.conditions).setVisibility(View.GONE);
                    next.setText("Total Estimated Price Rp."+0);
                }
                break;
            case 1:
                next.setEnabled(order.getImages().size()>0);

                break;
            case 2:

                    next.setEnabled(order.getMixMatchQuantity()==order.getQuantity());

                    if(order.getMixMatchQuantity()>order.getQuantity()){
                        Toast.makeText(getContext(), R.string.warning_mixmatch_greater, Toast.LENGTH_SHORT).show();
                    }

                    ((TextView)view.findViewById(R.id.remaining_quantity)).setText("Remaining Quantity Not yet Assign: "+(order.getQuantity()-order.getMixMatchQuantity()));

                    if(order.getMixMatchQuantity()<order.getQuantity()) {
                        view.findViewById(R.id.add_variation).setVisibility(View.VISIBLE);
                    }else {
                        view.findViewById(R.id.add_variation).setVisibility(View.INVISIBLE);
                    }
                break;
            case 3:

                next.setEnabled(sizeChartConfirm.isChecked());
                break;
            case 4:

                next.setEnabled(accessoriesConfirm.isChecked());
                break;
        }

        footer.setEnabled(next.isEnabled());

        if(next!=null)
            next.setText("Total Estimated Price Rp."+Util.cf.format(totalEstimate(order)));
        if(step>=0 && step<=4)
            ((TextView)view.findViewById(R.id.cart_total)).setText(""+totalOrders);

    }

    private class MyCheckedChangeListner implements CompoundButton.OnCheckedChangeListener {
        RadioButton[] radios;
        public MyCheckedChangeListner(RadioButton[] buttons) {
            radios=buttons;
        }
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if(!b)
                return;


            for (int i=0;i<radios.length;i++){
                RadioButton radio=radios[i];
                if (radio.getId()!=compoundButton.getId()){
                    radio.setChecked(false);
                }else {



                    if(radio.getTag().toString().equals("button"))
                        order.setButton(i);
                    else if(radio.getTag().toString().equals("else"))
                        order.setZipper(i);
                    else order.setFuring(i);

                    if(radio.getTag().toString().equals("furing"))
                        validateSteps();
                }
            }
        }
    }


    public static long totalEstimate(SewingOrder newOrder){
        long totalPrice=0;
        totalItems=0;

        List<SewingOrder> orders=SewingOrder.listAll(SewingOrder.class);
        totalOrders=orders.size();
        for(SewingOrder order:orders){
            if(order.getId()==newOrder.getId())
                continue;
            totalItems+=order.getQuantity();
        }
        totalItems+=newOrder.getQuantity();
        for(SewingOrder order:orders){
            if(order.getId()==newOrder.getId())
                continue;
            totalPrice+=order.getNetPrice()+order.getFuringPrice();
        }
        totalPrice+=newOrder.getNetPrice()+newOrder.getFuringPrice();



        return (long) (totalPrice*(1-Util.transactionDiscount(totalItems)));
    }

}
