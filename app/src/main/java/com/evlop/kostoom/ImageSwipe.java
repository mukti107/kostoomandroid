package com.evlop.kostoom;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.evlop.kostoom.adapters.ImageSwipeAdapter;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by rh on 12/8/16.
 */
public class ImageSwipe extends AppCompatActivity implements View.OnClickListener {
    ImageSwipeAdapter imageSwipeAdapter;

    Button faq,letMeIn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.I);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        setContentView(R.layout.image_swipe);
        ViewPager vpPager = (ViewPager) findViewById(R.id.image_swipe);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        imageSwipeAdapter = new ImageSwipeAdapter(getSupportFragmentManager());

        switch (MainActivity.applicationData(getApplicationContext()).getString("user-type",Role.TAILOR)) {
            case Role.CUSTOMER:

                imageSwipeAdapter.setBoarding(new BoardingImage[]{
                        new BoardingImage(R.mipmap.boarding_customer_1,R.string.boarding_customer_1_title,R.string.boarding_customer_1_description),
                        new BoardingImage(R.mipmap.boarding_customer_2,R.string.boarding_customer_2_title,R.string.boarding_customer_2_description),
                        new BoardingImage(R.mipmap.boarding_customer_3,R.string.boarding_customer_3_title,R.string.boarding_customer_3_description),
                });
                //imageSwipeAdapter.setImages(new int[]{R.mipmap.boarding_customer_1,R.mipmap.boarding_customer_2,R.mipmap.boarding_customer_3});
                break;
            case Role.TAILOR:
                imageSwipeAdapter.setBoarding(new BoardingImage[]{
                        new BoardingImage(R.mipmap.boarding_tailor_1,R.string.boarding_tailor_1_title,R.string.boarding_tailor_1_description),
                        new BoardingImage(R.mipmap.boarding_tailor_2,R.string.boarding_tailor_2_title,R.string.boarding_tailor_2_description),
                        new BoardingImage(R.mipmap.boarding_tailor_3,R.string.boarding_tailor_3_title,R.string.boarding_tailor_3_description),

                });
        }

        vpPager.setAdapter(imageSwipeAdapter);
        indicator.setViewPager(vpPager);

        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
             if(position>=2)
                 letMeIn.setVisibility(View.VISIBLE);
            }
            @Override
            public void onPageSelected(int position) {
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        faq=(Button)findViewById(R.id.faq);
        letMeIn=(Button)findViewById(R.id.let_me_in);
//        faq.setOnClickListener(this);
        letMeIn.setOnClickListener(this);
        letMeIn.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.let_me_in:
                startActivity(new Intent(this,MainActivity.class));
                finish();
                break;
        }
    }

    public class BoardingImage {
        int image,title,description;
        public BoardingImage(int image, int title, int description) {
            this.image=image;
            this.title=title;
            this.description=description;
        }

        public int getImage() {
            return image;
        }

        public int getTitle() {
            return title;
        }

        public int getDescription() {
            return description;
        }
    }
}
