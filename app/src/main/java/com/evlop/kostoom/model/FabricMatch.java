package com.evlop.kostoom.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.util.ArrayList;

/**
 * Created by rh on 12/10/16.
 */
public class FabricMatch extends SugarRecord{
    @Expose
    ArrayList<Image> images=new ArrayList<>();
    @Expose
    int type;
    @Expose
    int quantity;

    @Expose(serialize = false, deserialize = false)
    Long sewingOrder;
    @Expose
    private String note;

    public ArrayList<Image> getImages() {
        if(images.size()==0) {
            for (Image i : Image.find(Image.class, "FABRIC_MATCH = ?", new String[]{String.valueOf(getId())})){
                images.add(i);
            }
        }
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setSewingOrder(Long sewingOrder) {
        this.sewingOrder = sewingOrder;
    }


    public Long getSewingOrder() {
        return sewingOrder;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
