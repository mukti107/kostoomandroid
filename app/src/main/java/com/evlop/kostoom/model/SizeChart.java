package com.evlop.kostoom.model;

import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;

/**
 * Created by rh on 12/10/16.
 */
public class SizeChart extends SugarRecord {
    @Expose
    int bust=0;
    @Expose
    int waist=0;
    @Expose
    int hips=0;
    @Expose
    int girth=0;
    @Expose
    int height=0;
    @Expose
    int others=0;

    public SizeChart(){}

    public SizeChart(int bust, int waste, int hips, int girth, int height, int others) {
        this.bust=bust;
        this.waist=waste;
        this.hips=hips;
        this.girth=girth;
        this.height=height;
        this.others=others;
    }

    public int getBust() {
        return bust;
    }

    public void setBust(int bust) {
        this.bust = bust;
    }

    public int getWaist() {
        return waist;
    }

    public void setWaist(int waist) {
        this.waist = waist;
    }

    public int getHips() {
        return hips;
    }

    public void setHips(int hips) {
        this.hips = hips;
    }

    public int getGirth() {
        return girth;
    }

    public void setGirth(int girth) {
        this.girth = girth;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getOthers() {
        return others;
    }

    public void setOthers(int others) {
        this.others = others;
    }
}
