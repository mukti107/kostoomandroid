package com.evlop.kostoom.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.evlop.kostoom.MainActivity;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Type;

/**
 * Created by rachindrapoudel on 12/18/16.
 */

public class Image extends SugarRecord {
    @Expose
    String image;
    @Expose(serialize = false, deserialize = false)
    long sewingOrder;
    @Expose(serialize = false, deserialize = false)
    long fabricMatch;

    public Image(){}
    public Image (Bitmap bmp){
        float ratio=(float)bmp.getHeight()/bmp.getWidth();
        int height = (ratio>1)?800: (int) (800.0f * ratio);
        int width = (ratio<1)?800: (int) (800.0f / ratio);

        Log.i("dimension",width+"::"+height+"::"+ratio);

        bmp=Bitmap.createScaledBitmap(bmp,width,height,false);
        saveImage(bmp);
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Bitmap getBitmap(){
        try{
            FileInputStream fis = MainActivity.context.openFileInput(image);
            Bitmap b = BitmapFactory.decodeStream(fis);
            fis.close();

            return b;
        }
        catch(Exception e){
        }
        return null;
    }

    public long getSewingOrder() {
        return sewingOrder;
    }

    public void setSewingOrder(long sewingOrder) {
        this.sewingOrder = sewingOrder;
    }

    public long getFabricMatch() {
        return fabricMatch;
    }

    public void setFabricMatch(long fabricMatch) {
        this.fabricMatch = fabricMatch;
    }


    public void saveImage(Bitmap b){
        String name="img_"+System.currentTimeMillis()+".bmp";
        FileOutputStream out;
        try {
            out = MainActivity.context.openFileOutput(name, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        image=name;
    }
}
