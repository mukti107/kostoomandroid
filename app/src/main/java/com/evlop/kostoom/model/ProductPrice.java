package com.evlop.kostoom.model;

import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;

public class ProductPrice extends SugarRecord {
    float p1,p2;

    public ProductPrice(){}

    public float getTotalPrice(int quantity,int total){
        return quantity*getPriceRate(quantity,total);
    }

    public float getPriceRate(int quantity,int total){

        if(total>=24 && quantity>=8)
            return p2;
        else
            return p1;
    }

    public float discount(int quantity){
        if(quantity<=15) return 0f;
        if(quantity>=16 && quantity<=23) return 0.05f;
        else if(quantity>=24 && quantity<=49) return 0.06f;
        else if(quantity>=50 && quantity<=99) return 0.08f;
        else if(quantity>=100 && quantity<=149) return 0.1f;
        else if(quantity>=250 && quantity<=499) return 0.2f;
        else return 0.25f;
    }

    public float[] getPriceAsArray(){
        return new float[]{p1,p2};
    }

    public void setRateFromJsonArray(JSONArray price) throws JSONException {

            this.p1= (float) price.getDouble(0);
            this.p2= (float) price.getDouble(1);
    }

    public float getP1() {
        return p1;
    }

    public void setP1(float p1) {
        this.p1 = p1;
    }

    public float getP2() {
        return p2;
    }

}
