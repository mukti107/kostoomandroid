package com.evlop.kostoom.model;

import android.telecom.Call;

import com.evlop.kostoom.adapters.ImageSerializer;
import com.evlop.kostoom.fragmets.ConfirmProfile;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rachindrapoudel on 1/26/17.
 */

public class Checkout {
    @Expose
    List<SewingOrder> orders;
    @Expose
    String pickupLocation="",deliveryLocation="",date="",time="",vehicle="";
    @Expose
    String distance;
    @Expose
    String address;
    @Expose
    private String[] details;
    public List<SewingOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<SewingOrder> orders) {
        this.orders = orders;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getDeliveryLocation() {
        return deliveryLocation;
    }

    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public void setDetails(String[] details) {
        this.details = details;
    }

    public Call setDetails(ConfirmProfile fragment) {
        return null;
    }

    public String[] getDetails() {
        return details;
    }


    public String toJson(){
        for(SewingOrder o: orders) {
            for (FabricMatch fm : o.getFabricMatch())
                fm.getImages();
        }

        Gson gs=new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(Image.class,new ImageSerializer()).create();
        return gs.toJson(this);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
