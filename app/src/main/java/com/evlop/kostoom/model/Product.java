package com.evlop.kostoom.model;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringWriter;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rh on 12/10/16.
 */
public class Product extends SugarRecord {


    String name;
    String imageUrl;
    String description;
    ProductPrice price=new ProductPrice();
    @Expose
    int itemId;

    @Expose
    JSONObject sizeChart=new JSONObject();

    @Expose
    String sizeChartData="";

    public Product(){}

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getImageUrl() {
        return imageUrl;
    }
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public ProductPrice getPrice() {
        return price;
    }

    public void setPrice(ProductPrice price) {
        this.price = price;
    }

    public void setItemId(int id) {
        this.itemId = id;
    }

    public int getItemId() {
        return itemId;
    }

    public void setSizeChartFromBackend(JSONObject size_chart) {
            try {
                for(int i=1;i<=size_chart.length();i++) {
                    JSONObject details=size_chart.getJSONObject(i + "");
                    String name=details.getString("name");
                    if(name.isEmpty())
                        continue;
                    JSONObject size=new JSONObject();
                        size.put("xs",details.getInt("xs"));
                        size.put("sm",details.getInt("sm"));
                        size.put("md",details.getInt("md"));
                        size.put("l",details.getInt("l"));
                        size.put("xl",details.getInt("xl"));
                    sizeChart.put(name,size);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    public void setSizeChart(JSONObject sizeChart) {
        this.sizeChart = sizeChart;
    }
    public JSONObject getSizeChart() {
        return sizeChart;
    }

    @Override
    public long save() {
        sizeChartData=sizeChart.toString();
        return super.save();
    }

    public String getSizeChartData() {
        return sizeChartData;
    }
}
