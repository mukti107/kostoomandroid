package com.evlop.kostoom.model;

import android.util.Log;

import com.evlop.kostoom.adapters.ImageSerializer;
import com.evlop.kostoom.functions.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.util.ArrayList;

@Table
public class SewingOrder extends SugarRecord{
    @Expose
    String type;
    @Expose
    Product item;
    @Expose
    boolean fabricWithCustomer=true;
    @Expose
    ArrayList<Image> images=new ArrayList<>();
    @Expose
    int quantity;
    SizeChart xs=new SizeChart(88,74,96,36,53,104),
              sm=new SizeChart(90,78,100,37,54,140),
              m =new SizeChart(94,82,104,38,56,140),
              l =new SizeChart(100,88,110,39,57,140),
              xl=new SizeChart(105,94,116,40,58,140);
    SizeChart[] sizeChart=new SizeChart[]{xs,sm,m,l,xl};
    @Expose
    int zipper=3,button=3,furing=0;
    @Expose
    String accessoriesNote="";
    @Expose(serialize = true)
    ArrayList<FabricMatch> fabricMatch=new ArrayList<>();
    @Expose
    private String designNotes="";
    @Expose
    double priceApplied;
    @Expose
    double furingPriceApplied;


    public SewingOrder(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Product getItem() {
        return item;
    }

    public void setItem(Product item) {
        this.item = item;
    }

    public ArrayList<Image> getImages() {
        Log.i("order","images"+images.size());
        if(images.size()==0) {
            for (Image i : Image.find(Image.class, "SEWING_ORDER = ?", new String[]{String.valueOf(getId())})) {
                images.add(i);
            }
        }
         return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public SizeChart[] getSizeChart() {
        return sizeChart;
    }

    public void setSizeChart(SizeChart[] sizeChart) {
        this.sizeChart = sizeChart;
    }

    public ArrayList<FabricMatch> getFabricMatch() {
        Log.i("order","images"+fabricMatch.size());
        if(fabricMatch.size()==0) {
            for (FabricMatch f : FabricMatch.find(FabricMatch.class, "SEWING_ORDER = ?", new String[]{String.valueOf(getId())})) {
                fabricMatch.add(f);
            }
        }
        return fabricMatch;
    }

    public void setFabricMatch(ArrayList<FabricMatch> fabricMatch) {
        this.fabricMatch = fabricMatch;
    }

    public boolean isFabricWithCustomer() {
        return fabricWithCustomer;
    }

    public void setFabricWithCustomer(boolean fabricWithCustomer) {
        this.fabricWithCustomer = fabricWithCustomer;
    }

    public String toJson(){

        for(FabricMatch fm:getFabricMatch())
            fm.getImages();
        getImages();

        Gson gs=new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(Image.class,new ImageSerializer()).create();
        return gs.toJson(this);
    }



    public int getRate() {
            if(applyBusinessPrice())
                return (int) item.getPrice().getP2();
            else
                return (int) item.getPrice().getP1();
    }

    public int getFuringRate(){
        if (furing>0)
            return 5000;
        else
            return 0;
    }

    private boolean applyBusinessPrice(){

        int total= Util.getCartTotal();
        if(getId()==null){
            total+=getQuantity();
        }
        for(FabricMatch m:fabricMatch){
            if(m.getQuantity()<8)
                return false;
        }

        if(total<24 || getQuantity()<8)
            return false;
        return true;
    }

    public int getPrice(int total) {
        return getRate()*getQuantity();
    }

    public float getDiscount(int total){
        return getQuantity()*getRate()*item.getPrice().discount(quantity);
    }



    public int getRateWithTransactionDiscount(){

        return (int) (((float)getNetPriceWithTransactionDiscount()/getPrice(0))*(getRate()));
    }

    public int getFuringPriceWithTransactionDiscount(){
        furingPriceApplied=  (((float)getNetPriceWithTransactionDiscount()/getPrice(0))*(getQuantity()*getFuringRate()));
        return (int) furingPriceApplied;
    }


    public boolean isMixMatchValid(){
        return getQuantity()==getMixMatchQuantity();
    }

    public int getMixMatchQuantity(){
        int mixMatchQuantity=0;
        for(FabricMatch match:fabricMatch){
            mixMatchQuantity+=match.getQuantity();
        }
        return mixMatchQuantity;
    }

    public static SewingOrder formJson(String jsonString) {
        Gson gs=new Gson();
        return gs.fromJson(jsonString,SewingOrder.class);
    }

    public int getZipper() {
        return zipper;
    }

    public void setZipper(int zipper) {
        this.zipper = zipper;
    }

    public int getButton() {
        return button;
    }

    public void setButton(int button) {
        this.button = button;
    }


    public String getAccessoriesNote() {
        return accessoriesNote;
    }

    public void setAccessoriesNote(String accessoriesNote) {
        this.accessoriesNote = accessoriesNote;
    }


    public SizeChart getXs() {
        return xs;
    }

    public void setXs(SizeChart xs) {
        this.xs = xs;
    }

    public SizeChart getSm() {
        return sm;
    }

    public void setSm(SizeChart sm) {
        this.sm = sm;
    }

    public SizeChart getM() {
        return m;
    }

    public void setM(SizeChart m) {
        this.m = m;
    }

    public SizeChart getL() {
        return l;
    }

    public void setL(SizeChart l) {
        this.l = l;
    }

    public SizeChart getXl() {
        return xl;
    }

    public void setXl(SizeChart xl) {
        this.xl = xl;
    }

    public int getFuring() {
        return furing;
    }

    public void setFuring(int furing) {
        this.furing = furing;
    }

    public double getNetPrice() {
        return getPrice(0)-getDiscount(0);
    }

    public double getNetPriceWithTransactionDiscount() {
        int total=Util.getCartTotal();
        if(getId()==null)
            total+=getQuantity();
        priceApplied= getNetPrice()*(1-Util.transactionDiscount(total));
        return priceApplied;
    }

    public void setDesignNotes(String designNotes) {
        this.designNotes = designNotes;
    }

    public String getDesignNotes() {
        return designNotes;
    }


    public double getFuringPrice() {
        return getQuantity()*getFuringRate();
    }
}
