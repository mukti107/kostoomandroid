package com.evlop.kostoom.components;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.evlop.kostoom.ImageFragment;
import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.adapters.ImageSwipeAdapter;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by rh on 12/8/16.
 */
public class ImageSlider {
    View root;
    int[] images;
    public ImageSlider(View root, int[] images, FragmentManager fm, int upload_design_images){
        this.images=images;
        this.root=root;
        ViewPager vpPager = (ViewPager) root.findViewById(upload_design_images);
        CircleIndicator indicator = (CircleIndicator) root.findViewById(R.id.indicator);
        vpPager.setAdapter(new SlideAdapter(fm));
        indicator.setViewPager(vpPager);

    }


    private class SlideAdapter extends FragmentPagerAdapter {

        public SlideAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            return new ImageFragment().withImage(images[position]);
        }

        @Override
        public int getCount() {
            return images.length;
        }
    }
}
