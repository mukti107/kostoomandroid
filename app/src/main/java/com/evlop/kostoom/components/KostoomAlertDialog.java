package com.evlop.kostoom.components;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.evlop.kostoom.R;

/**
 * Created by rachindrapoudel on 1/23/17.
 */

public class KostoomAlertDialog implements View.OnClickListener {

    AppCompatActivity context;
    AlertDialogActions actions=null;
    android.support.v7.app.AlertDialog alertDialog;
    ImageView iconView,close;
    Button yesButton, noButton;
    TextView titleView, messageView;

    String title, message,noText="no",yesText="yes";
    int icon=R.mipmap.ic_launcher;

    public KostoomAlertDialog(){

    }

    public void show(){
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_component, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        yesButton =(Button)dialogView.findViewById(R.id.yes);
        noButton =(Button)dialogView.findViewById(R.id.no);
        close=(ImageView) dialogView.findViewById(R.id.close);

        iconView =(ImageView) dialogView.findViewById(R.id.icon);

        titleView =(TextView) dialogView.findViewById(R.id.title);
        messageView =(TextView) dialogView.findViewById(R.id.message);

        messageView.setText(message);
        iconView.setImageResource(icon);
        titleView.setText(title);

        if(!yesText.isEmpty()) {
            yesButton.setVisibility(View.VISIBLE);
            yesButton.setText(yesText);
        }

        if(!noText.isEmpty()) {
            noButton.setVisibility(View.VISIBLE);
            noButton.setText(noText);
        }

        yesButton.setOnClickListener(this);
        noButton.setOnClickListener(this);
        close.setOnClickListener(this);
        alertDialog.show();
    }

    public KostoomAlertDialog setActivity(Activity activity){
        this.context= (AppCompatActivity) activity;
        return this;
    }

    private Activity getActivity() {
        return context;
    }

    public void setActions(AlertDialogActions actions) {
        this.actions = actions;
    }

    @Override
    public void onClick(View view) {
        if(actions!=null)
            switch (view.getId()){
                case R.id.yes:
                    actions.onPositiveAction();
                    break;
                case R.id.no:
                    actions.onNegativeAction();
            }

        alertDialog.dismiss();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNoText() {
        return noText;
    }

    public void setNoText(String noText) {
        this.noText = noText;
    }

    public String getYesText() {
        return yesText;
    }

    public void setYesText(String yesText) {
        this.yesText = yesText;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}