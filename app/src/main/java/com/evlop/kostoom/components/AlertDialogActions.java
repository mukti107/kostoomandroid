package com.evlop.kostoom.components;

/**
 * Created by rachindrapoudel on 1/23/17.
 */

public interface AlertDialogActions {

    public void onPositiveAction();
    public void onNegativeAction();
}
