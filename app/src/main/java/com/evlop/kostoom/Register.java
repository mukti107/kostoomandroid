package com.evlop.kostoom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evlop.kostoom.async.UserRegister;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Created by rh on 12/6/16.
 */
public class Register extends AppCompatActivity implements View.OnClickListener {

    CallbackManager callbackManager;
    TextView signin;
    Button register;
    EditText username,password,phone;
    boolean passwordVisible=false;
    private ImageView passwordShow;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window =getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorAccent));
        }

        setContentView(R.layout.register);

        callbackManager = CallbackManager.Factory.create();
        register=(Button)findViewById(R.id.register);
        signin=(TextView)findViewById(R.id.signin);

        username=(EditText)findViewById(R.id.username);
        password=(EditText)findViewById(R.id.password);
        phone=(EditText)findViewById(R.id.phone);

        passwordShow=(ImageView)findViewById(R.id.password_show);
        passwordShow.setOnClickListener(this);

        register.setOnClickListener(this);
        signin.setOnClickListener(this);


        LoginManager.getInstance().registerCallback(callbackManager, Login.facebookLogin(this));

        Button facebookLogin = (Button)findViewById(R.id.facebook_login);
        facebookLogin.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signin:
                startActivity(new Intent(getApplicationContext(),Login.class));
                finish();
                break;
            case R.id.register:

                if(!Patterns.PHONE.matcher(phone.getText().toString()).matches()) {
                    Toast.makeText(getApplicationContext(), R.string.invalid_phone_format, Toast.LENGTH_LONG).show();
                    return;
                }
                new UserRegister(this,username.getText().toString(),
                        password.getText().toString(),
                        phone.getText().toString()
                        ).execute();
                break;
            case R.id.facebook_login:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
                break;
            case R.id.password_show:



                if(passwordVisible)
                    password.setInputType(129);
                else
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                passwordVisible=!passwordVisible;
                passwordShow.setAlpha(passwordVisible?0.5f:1f);

                break;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
