package com.evlop.kostoom.async;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.evlop.kostoom.Login;
import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.Register;
import com.evlop.kostoom.Role;
import com.evlop.kostoom.functions.HttpTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by rh on 11/9/16.
 */
public class UserRegister extends AsyncTask<String, Void, String> {
    private final String phone;
    String username,password;
    Context context;
    ProgressDialog progress;
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(context, "Registering User", "Please Wait...", true);
    }

    public UserRegister(Context context, String user, String password, String phone){
        this.username=user;
        this.password=password;
        this.phone=phone;
        this.context=context;

    }

    @Override
    protected String doInBackground(String... strings) {
        String source= null;
        try {
            source = HttpTask.baseURL+"/user/register?email="+ URLEncoder.encode(username,"utf-8")+"&password="+password+"&pone="+phone;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return HttpTask.get(source);
    }

    protected void onPostExecute(String result) {
            progress.dismiss();
        try {
            JSONObject response=new JSONObject(result);
            if(response.getBoolean("success")){
                Toast.makeText(context,"User registered succesfully",Toast.LENGTH_LONG).show();
                Intent roleActivity =new Intent(context, Role.class);
                roleActivity.putExtra("username",username);
                roleActivity.putExtra("password",password);
                context.startActivity(roleActivity);
                ((Register)context).finish();
            }
            else{
                String error="Error:";
                JSONArray messages=response.getJSONArray("messages");
                for(int i=0;i<messages.length();i++)
                    error+="\n"+messages.getString(i);

                Toast.makeText(context,error,Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context,"Network Error",Toast.LENGTH_LONG).show();

        }

    }
}