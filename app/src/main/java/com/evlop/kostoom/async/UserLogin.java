package com.evlop.kostoom.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.Role;
import com.evlop.kostoom.functions.HttpTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rh on 11/9/16.
 */
public class UserLogin extends AsyncTask<String, Void, String> {
    SharedPreferences sharedPreferences;
    Context context;
    String username,password,token;
    ProgressDialog progress;
    private Intent intent;


    public UserLogin(Context context,String user, String password){
        this.username=user;
        this.password=password;
        this.context=context;
        sharedPreferences= MainActivity.applicationData(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(context, "Logging In", "Please Wait...", true);
        Log.i("progress","show");
    }

    @Override
    protected String doInBackground(String... strings) {
        String source=HttpTask.baseURL+"/user/login?email="+username+"&password="+password+"&token_fb="+token;
        return HttpTask.get(source);
    }

    @Override
    protected void onPostExecute(String result) {
        progress.dismiss();
        JSONObject user= null;
        try {
            user = new JSONObject(result);
            if(user.getBoolean("success")) {
                sharedPreferences.edit().putString("user-token", user.getString("token")).apply();
                sharedPreferences.edit().putString("user-type", user.getString("type")).apply();
                Log.i("usertype",user.getString("type"));
                if(user.getString("type").isEmpty() || user.getString("type")==null || user.getString("type").equals("null"))
                {
                    Intent roleActivity =new Intent(context, Role.class);
                    roleActivity.putExtra("username",username);
                    roleActivity.putExtra("password",password);
                    roleActivity.putExtra("token",token);
                    roleActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(roleActivity);
                }else {
                    sharedPreferences.edit().putBoolean("user-logged", true).apply();
                    Intent intent = (this.intent != null) ? this.intent : new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                ((AppCompatActivity)context).finish();
            }else{
                String error="Error:";
                JSONArray messages=user.getJSONArray("messages");
                for(int i=0;i<messages.length();i++)
                    error+="\n"+messages.getString(i);

                Toast.makeText(context,error,Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public void setToken(String token) {
        this.token = token;
    }
}