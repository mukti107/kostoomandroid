package com.evlop.kostoom.async;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.functions.HttpTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rh on 11/9/16.
 */
public class SaveMyAccount extends AsyncTask<String, Void, String> {
    SharedPreferences sharedPreferences;
    MainActivity context;
    ProgressDialog progress;


    public SaveMyAccount(MainActivity mainActivity) {
        this.context=mainActivity;
        sharedPreferences=MainActivity.applicationData(mainActivity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(context, "Processing", "Please Wait...", true);    }

    @Override
    protected String doInBackground(String... strings) {
        String source=HttpTask.baseURL+"/user/updatedetails?token="+sharedPreferences.getString("user-token","")+"&"+strings[0];
        return HttpTask.get(source);
    }

    @Override
    protected void onPostExecute(String result) {
        progress.dismiss();
    }
}