package com.evlop.kostoom.async;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.fragmets.*;
import com.evlop.kostoom.fragmets.MyOffers;
import com.evlop.kostoom.functions.HttpTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rh on 11/9/16.
 */
public class FetchOrder extends AsyncTask<String, Void, String> {
    SharedPreferences sharedPreferences;
    MainActivity context;
    String id;
    ProgressDialog progress;
    Fragment returnFragment=null;


    public FetchOrder(MainActivity mainActivity, String id) {
        this.context=mainActivity;
        this.sharedPreferences=MainActivity.applicationData(mainActivity.getApplicationContext());
        this.id=id;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(context, "Processing", "Please Wait...", true);
    }

    @Override
    protected String doInBackground(String... strings) {
        String source=HttpTask.baseURL+"/order/details/"+id+"?token="+sharedPreferences.getString("user-token","");
        return HttpTask.get(source);
    }

    @Override
    protected void onPostExecute(String result) {
        progress.dismiss();

        try {
            context.openFargment(new ProjectDetails().withOrder(new JSONObject(result)).fromFragment(returnFragment));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public FetchOrder withReturn(Fragment myOffers) {
        returnFragment=myOffers;
        return this;
    }
}