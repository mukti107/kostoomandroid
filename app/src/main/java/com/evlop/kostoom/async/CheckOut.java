package com.evlop.kostoom.async;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.functions.HttpTask;
import com.evlop.kostoom.model.Checkout;
import com.evlop.kostoom.model.SewingOrder;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by rh on 11/9/16.
 */
public abstract class CheckOut extends AsyncTask<String, Void, String> {
    private final List<SewingOrder> orders;
    private final String[] details;
    SharedPreferences sharedPreferences;
    Context context;
    private ProgressDialog progress;
    Checkout checkoutData;


    public CheckOut(Context context,Checkout checkout){
        this.orders=checkout.getOrders();
        this.context=context;
        sharedPreferences= MainActivity.applicationData(context);
        this.details=checkout.getDetails();
        checkoutData=checkout;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(context, "Processing checkout", "Please Wait...", true);
        Log.i("progress","show");
    }

    @Override
    protected String doInBackground(String... strings) {

        String personalDetails= null;
        try {
            personalDetails = "name="+ URLEncoder.encode(details[0],"utf-8");
            personalDetails+="&phone="+URLEncoder.encode(details[1],"utf-8");
            personalDetails+="&email="+URLEncoder.encode(details[2],"utf-8");
            personalDetails+="&address1="+URLEncoder.encode(details[3],"utf-8");
            personalDetails+="&city="+URLEncoder.encode(details[4],"utf-8");
            personalDetails+="&address2="+URLEncoder.encode(details[5],"utf-8");
            personalDetails+="&brand="+URLEncoder.encode(details[6],"utf-8");
            HttpTask.get(HttpTask.baseURL+"/order/contactdetails?token="+sharedPreferences.getString("user-token","")+"&"+personalDetails);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        try {
            String source=HttpTask.baseURL+"/order";
            String resp=HttpTask.post(source,"token="+sharedPreferences.getString("user-token","")+"&transaction="+ URLEncoder.encode(checkoutData.toJson(),"utf-8"));
            JSONObject response=new JSONObject(resp);
            for(SewingOrder order:orders){
                order.delete();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

//        for(SewingOrder order:orders){
//            String source=HttpTask.baseURL+"/order";
//            String odr=order.toJson();
//            Log.i("orderjson",odr);
//            try {
//                String resp=HttpTask.post(source,"token="+sharedPreferences.getString("user-token","")+"&order="+ URLEncoder.encode(odr,"utf-8"));
//                JSONObject response=new JSONObject(resp);
//                if(response.getBoolean("success")){
//                    order.delete();
//                }
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        progress.dismiss();
        showSuccess();
        JSONObject user= null;

    }

    protected abstract void showSuccess();


//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
//        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
//        View dialogView = inflater.inflate(R.layout.checkout_success, null);
//        dialogBuilder.setView(dialogView);
//        final AlertDialog alertDialog = dialogBuilder.create();
//        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        alertDialog.show();
//    }


}