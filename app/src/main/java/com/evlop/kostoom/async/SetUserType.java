package com.evlop.kostoom.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.evlop.kostoom.ImageSwipe;
import com.evlop.kostoom.functions.HttpTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by rh on 11/9/16.
 */
public class SetUserType extends AsyncTask<String, Void, String> {
    String facebookToken;
    String username,password,type;
    Context context;
    ProgressDialog progress;
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(context, "Processing", "Please Wait...", true);
    }

    public SetUserType(Context context, String user, String type, String password, String facebookToken){
        this.username=user;
        this.type=type;
        this.context=context;
        this.password=password;
        this.facebookToken=facebookToken;
    }

    @Override
    protected String doInBackground(String... strings) {
        String source= null;
        try {
            source = HttpTask.baseURL+"/user/type?email="+ URLEncoder.encode(username,"utf-8")+"&type="+type;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return HttpTask.get(source);
    }

    protected void onPostExecute(String result) {
            progress.dismiss();
        try {
            JSONObject response=new JSONObject(result);
            if(response.getBoolean("success")){
                UserLogin login= new UserLogin(context,username,password);
                login.setIntent(new Intent(context, ImageSwipe.class));
                login.setToken(facebookToken);
                login.execute();
            }
            else{
                String error="Error:";
                JSONArray messages=response.getJSONArray("messages");
                for(int i=0;i<messages.length();i++)
                    error+="\n"+messages.getString(i);

                Toast.makeText(context,error,Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context,"Network Error",Toast.LENGTH_LONG).show();
        }

    }
}