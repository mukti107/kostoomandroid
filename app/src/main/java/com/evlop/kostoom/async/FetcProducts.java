package com.evlop.kostoom.async;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.fragmets.ItemList;
import com.evlop.kostoom.fragmets.PriceList;
import com.evlop.kostoom.functions.HttpTask;
import com.evlop.kostoom.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by rh on 11/9/16.
 */
public class FetcProducts extends AsyncTask<String, Void, String> {
    SharedPreferences sharedPreferences;
    MainActivity context;
    ProgressDialog progress;
    Fragment showIn;


    public FetcProducts(MainActivity mainActivity) {
        this.context=mainActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(context, "Retriving Products", "Please Wait...", true);
    }

    @Override
    protected String doInBackground(String... strings) {
        String source=HttpTask.baseURL+"/list/products";
        return HttpTask.get(source);
    }

    @Override
    protected void onPostExecute(String result) {
        progress.dismiss();
        ArrayList<Product> productList=new ArrayList<>();

        try {
            JSONArray products=new JSONArray(result);
            for(int i=0;i<products.length();i++){
                JSONObject prod=products.getJSONObject(i);
                Product product=new Product();
                product.setItemId(prod.getInt("id"));
                product.setName(prod.getString("name"));
                product.setDescription(prod.getString("description"));
                product.setImageUrl(HttpTask.baseURL+prod.getString("image"));
                product.getPrice().setRateFromJsonArray(prod.getJSONArray("price"));
                product.setSizeChartFromBackend(prod.getJSONObject("size_chart"));
                productList.add(product);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(showIn instanceof PriceList)
            context.openFargment(new PriceList().withProducts(productList));
        else
            context.openFargment(new ItemList().withProducts(productList));
    }

    public FetcProducts showIn(Fragment fragment){
        this.showIn=fragment;
        return this;
    }

}