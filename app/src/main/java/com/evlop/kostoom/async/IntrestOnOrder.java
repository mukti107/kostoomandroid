package com.evlop.kostoom.async;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.fragmets.ProjectDetails;
import com.evlop.kostoom.functions.HttpTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rh on 11/9/16.
 */
public class IntrestOnOrder extends AsyncTask<String, Void, String> {
    private final boolean intrest;
    SharedPreferences sharedPreferences;
    MainActivity context;
    String id;
    ProgressDialog progress;


    public IntrestOnOrder(MainActivity mainActivity, String id,boolean intrest) {
        this.context=mainActivity;
        this.intrest=intrest;
        this.sharedPreferences=MainActivity.applicationData(mainActivity.getApplicationContext());
        this.id=id;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(context, "Processing", "Please Wait...", true);
    }

    @Override
    protected String doInBackground(String... strings) {
        String source=HttpTask.baseURL+"/order/intrestedtailor/"+id+"?token="+sharedPreferences.getString("user-token","")+"&order_id="+id+"&flag="+(intrest?1:0);
        return HttpTask.get(source);
    }

    @Override
    protected void onPostExecute(String result) {
        progress.dismiss();

        try {
            context.openFargment(new ProjectDetails().withOrder(new JSONObject(result).getJSONObject("updated_order")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}