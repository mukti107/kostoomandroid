package com.evlop.kostoom.async;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.fragmets.ProjectFeeds;
import com.evlop.kostoom.functions.HttpTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rh on 11/9/16.
 */
public class MyAccount extends AsyncTask<String, Void, String> {
    SharedPreferences sharedPreferences;
    MainActivity context;
    ProgressDialog progress;
    boolean profile=true;


    public MyAccount(MainActivity mainActivity) {
        this.context=mainActivity;
        sharedPreferences=MainActivity.applicationData(mainActivity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(context, "Processing", "Please Wait...", true);    }

    @Override
    protected String doInBackground(String... strings) {
        String source=HttpTask.baseURL+"/user/details?token="+sharedPreferences.getString("user-token","");
        return HttpTask.get(source);
    }

    @Override
    protected void onPostExecute(String result) {
        progress.dismiss();
        try {
            if(profile)
                context.openFargment(new com.evlop.kostoom.fragmets.MyProfile().with(new JSONObject(result)));
            else
                context.openFargment(new com.evlop.kostoom.fragmets.MyAccount().with(new JSONObject(result)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public MyAccount setProfile(boolean profile) {
        this.profile = profile;
        return this;
    }
}