package com.evlop.kostoom;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by rh on 12/8/16.
 */
public class ImageFragment extends Fragment {

    int image;
    public ImageFragment(){

    }

    public ImageFragment withImage(int pos){
        this.image =pos;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View imageView=inflater.inflate(R.layout.image,null);
        ((ImageView)imageView.findViewById(R.id.image)).setImageResource(image);
        return imageView;
    }
}

