package com.evlop.kostoom;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.evlop.kostoom.async.FetcProducts;
import com.evlop.kostoom.async.MyAccount;
import com.evlop.kostoom.async.MyOffers;
import com.evlop.kostoom.async.MyOrders;
import com.evlop.kostoom.async.ProjectFeed;
import com.evlop.kostoom.components.AlertDialogActions;
import com.evlop.kostoom.components.KostoomAlertDialog;
import com.evlop.kostoom.fragmets.BusinessSewing;
import com.evlop.kostoom.fragmets.Cart;
import com.evlop.kostoom.fragmets.Checkout;
import com.evlop.kostoom.fragmets.Invoice;
import com.evlop.kostoom.fragmets.ItemList;
import com.evlop.kostoom.fragmets.Map;
import com.evlop.kostoom.fragmets.PaymentInfo;
import com.evlop.kostoom.fragmets.PriceList;
import com.evlop.kostoom.fragmets.PrivacyPolicy;
import com.evlop.kostoom.fragmets.ProjectFeeds;
import com.evlop.kostoom.fragmets.Payment;
import com.evlop.kostoom.interfaces.GoBack;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.prechat.ZopimChatActivity;

import java.util.LinkedList;
import java.util.Queue;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static Context context;
    public Queue<Fragment> previous=new LinkedList<>();
    public Fragment current;
    public Queue<Fragment> next=new LinkedList<>();

    public static MainActivity application;
    public ImageLoader imageLoader;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_LEFT_ICON);
        setContentView(R.layout.activity_main);


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/arial.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        ZopimChat.init("4P3uAuzl7OP7V7zZ9rQkHch4NhYqKhMq");


        configImageLoader();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((GoBack)current).goBack();

            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        Role.prepareMenu(applicationData(this).getString("user-type",Role.CUSTOMER),navigationView.getMenu());

        navigationView.setNavigationItemSelectedListener(this);
        context=getBaseContext();

        openHome();

    }

    private void configImageLoader() {


        DisplayImageOptions opts = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(opts).build();
        ImageLoader.getInstance().init(config);
        imageLoader = ImageLoader.getInstance();
    }

    public void openHome() {
        switch (applicationData(getApplicationContext()).getString("user-type",Role.TAILOR)) {
            case Role.CUSTOMER:
                openFargment(new ItemList());
                break;
            case Role.TAILOR:
                openFargment(new ProjectFeeds());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if(current instanceof GoBack){
            ((GoBack)current).goBack();
        }
        else if(current instanceof ItemList || current instanceof ProjectFeeds){
            KostoomAlertDialog dialog=new KostoomAlertDialog().setActivity(this);
            dialog.setMessage("Do you want to Leave Kostoom?");
            dialog.setTitle("Exit app?");
            dialog.setYesText("Yes");
            dialog.setNoText("No");
            dialog.setActions(new AlertDialogActions() {
                @Override
                public void onPositiveAction() {
                    MainActivity.this.finish();
                }
                @Override public void onNegativeAction() {
                }
            });
            dialog.show();
        }else{
            openHome();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int activateMenu = R.menu.main;
        getMenuInflater().inflate(activateMenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            default:
                if(current instanceof GoBack)
                    ((GoBack)current).backToHome();
                else
                    openHome();

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        final int id = item.getItemId();

        if(!(current instanceof BusinessSewing))
            handleNavMenu(id);
        else {
            KostoomAlertDialog dialog=new KostoomAlertDialog().setActivity(this);
            dialog.setMessage("Are you sure you want to leave the order process? Your order is not saved yet.");
            dialog.setTitle("Discard order?");

            dialog.setActions(new AlertDialogActions() {
                @Override
                public void onPositiveAction() {
                    handleNavMenu(id);
                }
                @Override public void onNegativeAction() {

                }
            });
            dialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }


    public void handleNavMenu(int id){
        switch (id){
            case R.id.nav_signout:
                if(Profile.getCurrentProfile()!=null)
                    LoginManager.getInstance().logOut();
                applicationData(this).edit().remove("user-logged").remove("user-token").remove("user-type").apply();
                startActivity(new Intent(getApplicationContext(),Login.class));
                finish();
                break;
            case R.id.nav_home:
                openHome();
                break;
            case R.id.nav_account:
                new MyAccount(MainActivity.this).execute();
                break;
            case R.id.nav_cart:
                openFargment(new Checkout());
                break;
            case R.id.nav_order:
                new MyOrders(MainActivity.this).execute();
                break;
            case R.id.nav_offer:
                new MyOffers(MainActivity.this).execute();
                break;
            case R.id.nav_payment:
                openFargment(new PaymentInfo());
                break;
            case R.id.nav_price_list:
                openFargment(new PriceList());
                break;
            case R.id.nav_support:
                startActivity(new Intent(getApplicationContext(), ZopimChatActivity.class));
                break;
            case R.id.nav_invoice:
                openFargment(new Invoice());
                break;
            case android.R.id.home:
                //Toast.makeText(getBaseContext(),"",Toast.LENGTH_LONG).show();
                break;
        }
    }

    public void clearSteps() {
        next.clear();
        previous.clear();
    }

    public static SharedPreferences applicationData(Context context) {
        return context.getSharedPreferences("data",0);
    }


    public void openFargment(Fragment fragment){
        current=fragment;
        if(fragment instanceof GoBack){
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.mipmap.ic_keyboard_arrow_left_white_24dp);
            toggle.syncState();
        }else {
            toggle.setDrawerIndicatorEnabled(true);
            toggle.syncState();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content,fragment).commit();
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

}
