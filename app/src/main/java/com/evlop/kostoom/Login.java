package com.evlop.kostoom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evlop.kostoom.async.UserLogin;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by rh on 12/6/16.
 */
public class Login extends AppCompatActivity implements View.OnClickListener {

    CallbackManager callbackManager;
    TextView forgotPassword,signup;
    EditText username,password;
    Button login;
    private ImageView passwordShow;
    boolean passwordVisible=false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(MainActivity.applicationData(this).getBoolean("user-logged",false)){
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.login);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window =getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorAccent));
        }

        forgotPassword=(TextView)findViewById(R.id.forgot_password);
        signup=(TextView)findViewById(R.id.signup);
        username=(EditText)findViewById(R.id.username);
        password=(EditText)findViewById(R.id.password);
        passwordShow=(ImageView)findViewById(R.id.password_show);
        passwordShow.setOnClickListener(this);

        login=(Button)findViewById(R.id.login);



        forgotPassword.setOnClickListener(this);
        signup.setOnClickListener(this);
        login.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, facebookLogin(this));


        Button facebookLogin = (Button)findViewById(R.id.facebook_login);
        facebookLogin.setOnClickListener(this);

        if(Profile.getCurrentProfile()!=null)
            LoginManager.getInstance().logOut();


    }

    public static FacebookCallback<LoginResult> facebookLogin(final AppCompatActivity context) {
        return new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                final String token=loginResult.getAccessToken().getToken();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                try {
                                    UserLogin login=new UserLogin(context,object.getString("email"),null);
                                    login.setToken(token);
                                    login.execute();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.login:
                new UserLogin(this,username.getText().toString(),password.getText().toString()).execute();
                break;
            case R.id.forgot_password:
                startActivity(new Intent(getApplicationContext(),ForgotPassword.class));
                break;
            case R.id.signup:
                startActivity(new Intent(getApplicationContext(),Register.class));
                break;
            case R.id.facebook_login:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
                break;
            case R.id.password_show:



                if(passwordVisible)
                    password.setInputType(129);
                else
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                passwordVisible=!passwordVisible;
                passwordShow.setAlpha(passwordVisible?0.5f:1f);

                break;

        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
