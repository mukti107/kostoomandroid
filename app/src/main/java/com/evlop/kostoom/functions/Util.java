package com.evlop.kostoom.functions;

import com.evlop.kostoom.model.SewingOrder;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by rachindrapoudel on 1/24/17.
 */

public class Util {
    public static DecimalFormat cf=new DecimalFormat("###,###,###,###,###,###");

    public static float transactionDiscount(int quantity){
        if(quantity>=100 && quantity<=199)
            return 0.03f;
        else if(quantity>=200 && quantity<=499)
            return 0.05f;
        else if(quantity>=500 && quantity<=999)
            return 0.08f;
        else if(quantity>=1000)
            return 0.1f;
        else return 0;
    }

    public static int getCartTotal() {
        List<SewingOrder> orders=SewingOrder.listAll(SewingOrder.class);
        int total=0;
        for(SewingOrder o:orders){
            total+=o.getQuantity();
        }
        return total;
    }
}
