package com.evlop.kostoom.functions;

import org.apache.http.client.ClientProtocolException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by rh on 12/6/16.
 */
public class HttpTask {
//    public static final String baseURL="http://192.168.100.5:8000";
  public static final String baseURL="http://163.172.165.10";
    public static String get(String sourceURL){
        String weather = "";
        try {
            URL url = new URL(sourceURL);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
            InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();

            String inputString;
            while ((inputString = bufferedReader.readLine()) != null) {
                builder.append(inputString);
            }

            weather=builder.toString();
            urlConnection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return weather;

    }
    public static String post(String postURL,String urlParameters){

        byte[] postData = urlParameters.getBytes(  );
        String res="";
        try {
            URL url = new URL(postURL);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");

            urlConnection.setDoOutput( true );
            urlConnection.setInstanceFollowRedirects( false );
            urlConnection.setRequestMethod( "POST" );
            //urlConnection.setRequestProperty( "Content-Type", "application/json");
            urlConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty( "charset", "utf-8");
            urlConnection.setRequestProperty( "Content-Length", Integer.toString( postData.length ));
            urlConnection.setUseCaches( false );

            DataOutputStream wr = new DataOutputStream( urlConnection.getOutputStream());
            wr.write( postData );



            InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();

            String inputString;
            while ((inputString = bufferedReader.readLine()) != null) {
                builder.append(inputString);
            }

            res=builder.toString();
            urlConnection.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
            res=null;
        }
        return res;
    }

}
