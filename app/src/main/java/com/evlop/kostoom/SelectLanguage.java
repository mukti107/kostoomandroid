package com.evlop.kostoom;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.evlop.kostoom.async.SetUserType;

import java.util.Locale;

/**
 * Created by rh on 12/6/16.
 */
public class SelectLanguage extends AppCompatActivity implements View.OnClickListener {



    Button english,indonesian;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window =getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorAccent));
        }
        english=(Button)findViewById(R.id.english);
        indonesian=(Button)findViewById(R.id.indonesian);
        english.setOnClickListener(this);
        indonesian.setOnClickListener(this);
        //defafult english
        onClick(english);
    }


    @Override
    public void onClick(View v) {
    String lang="en";
        switch (v.getId()){
            case R.id.english:
                lang="en";
                break;
            case R.id.indonesian:
                lang="in";
                break;
        }
        MainActivity.applicationData(this).edit().putString("langage",lang).apply();

        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(lang);
        res.updateConfiguration(conf, dm);

        if(MainActivity.applicationData(this).getBoolean("user-logged",false)){
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }else {
            startActivity(new Intent(this,Login.class));
            finish();
        }
    }


}
