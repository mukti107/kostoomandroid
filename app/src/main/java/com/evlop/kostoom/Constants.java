package com.evlop.kostoom;

/**
 * Created by rh on 12/12/16.
 */
public class Constants {
    public static final int REQUEST_CAMERA=1;
    public static final int REQUEST_FILE=2;


    public static final int SIZE_EXTRA_SMALL=0;
    public static final int SIZE_SMALL=1;
    public static final int SIZE_MEDIUM=2;
    public static final int SIZE_LARGE=3;
    public static final int SIZE_EXTRA_LARGE=4;



}
