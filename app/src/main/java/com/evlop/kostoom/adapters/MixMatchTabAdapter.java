package com.evlop.kostoom.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.evlop.kostoom.Constants;
import com.evlop.kostoom.R;
import com.evlop.kostoom.fragmets.AddMixMatchFragment;
import com.evlop.kostoom.fragmets.BusinessSewing;
import com.evlop.kostoom.fragmets.MixMatchSummaryFragment;
import com.evlop.kostoom.fragmets.SizeChartFragment;
import com.evlop.kostoom.model.SizeChart;

/**
 * Created by rh on 12/8/16.
 */
public class MixMatchTabAdapter extends FragmentPagerAdapter {
    public int images[] =new int[]{R.mipmap.ic_launcher,R.drawable.com_facebook_auth_dialog_background};
    SizeChart[] sizeCharts;
    Context context;
    private BusinessSewing businessSewing;

    public MixMatchTabAdapter(FragmentManager supportFragmentManager, Context context) {
        super(supportFragmentManager);
        this.context=context;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0)
        return new AddMixMatchFragment().withInstance(businessSewing);
        else
            return new MixMatchSummaryFragment().withInstane(businessSewing);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        int res=0;
        switch (position){
            case 0:
                return "Fabrics Variation";
            default:
                return "Summary";
        }
    }

    public MixMatchTabAdapter withInstance(BusinessSewing businessSewing) {
        this.businessSewing=businessSewing;
        return this;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
