package com.evlop.kostoom.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.evlop.kostoom.Constants;
import com.evlop.kostoom.ImageFragment;
import com.evlop.kostoom.R;
import com.evlop.kostoom.fragmets.SizeChartFragment;
import com.evlop.kostoom.model.FabricMatch;
import com.evlop.kostoom.model.SewingOrder;
import com.evlop.kostoom.model.SizeChart;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by rh on 12/8/16.
 */
public class SizeChartAdapter extends FragmentPagerAdapter {
    public int images[] =new int[]{R.mipmap.ic_launcher,R.drawable.com_facebook_auth_dialog_background};
    JSONObject sizeCharts;
    String[] labels;
    Context context;
    private String[] abbrev;

    public SizeChartAdapter(FragmentManager supportFragmentManager, Context context) {
        super(supportFragmentManager);
        this.context=context;
    }

    @Override
    public Fragment getItem(int position) {
        return new SizeChartFragment().with(sizeCharts,abbrev[position]);
    }

    @Override
    public int getCount() {
        return this.labels.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return labels[position];
    }
    public SizeChartAdapter with(SewingOrder order) {
        this.sizeCharts=order.getItem().getSizeChart();
        ArrayList<String> filtered=new ArrayList<>();
        ArrayList<String> abbrev=new ArrayList<>();
        ArrayList<SizeChart> filteredChart=new ArrayList<>();
        for (FabricMatch match:order.getFabricMatch()){
            if(!filtered.contains(sizeChartString(match.getType()))) {
                filtered.add(sizeChartString(match.getType()));
                abbrev.add(sizeChartAbbrev(match.getType()));
                filteredChart.add(order.getSizeChart()[match.getType()]);
            }
        }
        this.labels=new String[filtered.size()];
        this.abbrev=new String[abbrev.size()];
        this.abbrev=abbrev.toArray(this.abbrev);
        //this.sizeCharts=new SizeChart[filteredChart.size()];
        this.labels= filtered.toArray(this.labels);
        //this.sizeCharts=  filteredChart.toArray(this.sizeCharts);
        return this;
    }


    private String sizeChartString(int size){
        int res=0;
        switch (size){
            case Constants.SIZE_EXTRA_SMALL:
                res=R.string.extra_small;
                break;
            case Constants.SIZE_SMALL:
                res=R.string.small;
                break;
            case Constants.SIZE_MEDIUM:
                res=R.string.medium;
                break;
            case Constants.SIZE_LARGE:
                res=R.string.large;
                break;
            case Constants.SIZE_EXTRA_LARGE:
                res=R.string.extra_large;
                break;
        }
        return context.getResources().getString(res);
    }

    private String sizeChartAbbrev(int size){
        String res="";
        switch (size){
            case Constants.SIZE_EXTRA_SMALL:
                res="xs";
                break;
            case Constants.SIZE_SMALL:
                res="sm";
                break;
            case Constants.SIZE_MEDIUM:
                res="md";
                break;
            case Constants.SIZE_LARGE:
                res="l";
                break;
            case Constants.SIZE_EXTRA_LARGE:
                res="xl";
                break;
        }
        return res;

    }

}
