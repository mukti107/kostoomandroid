package com.evlop.kostoom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.evlop.kostoom.R;
import com.evlop.kostoom.model.FabricMatch;

/**
 * Created by rh on 12/13/16.
 */
public class MixMatchAdapter extends BaseAdapter {
    private final Context context;
    private final FabricMatch mixMatch;
    private final LayoutInflater inflater;

    public MixMatchAdapter(Context context, FabricMatch fabricMatch) {
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context=context;
        this.mixMatch=fabricMatch;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=inflater.inflate(R.layout.add_mix_match_record,null);
        return view;
    }
}
