package com.evlop.kostoom.adapters;

import android.graphics.Bitmap;
import android.util.Base64;

import com.evlop.kostoom.model.Image;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;

/**
 * Created by rachindrapoudel on 12/19/16.
 */

public class ImageSerializer implements JsonSerializer<Image> {
    @Override
    public JsonElement serialize(Image image, Type typeOfSrc, JsonSerializationContext context) {
        String result="";
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.getBitmap().compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        result=Base64.encodeToString(byteArray, Base64.DEFAULT);
        return new JsonPrimitive(result);
    }
}
