package com.evlop.kostoom.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.evlop.kostoom.ImageFragment;
import com.evlop.kostoom.ImageSwipe;
import com.evlop.kostoom.R;
import com.evlop.kostoom.fragmets.BoardingImageFragment;

/**
 * Created by rh on 12/8/16.
 */
public class ImageSwipeAdapter extends FragmentPagerAdapter {
    private ImageSwipe.BoardingImage[] boardingImages;

    public ImageSwipeAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        return new BoardingImageFragment().withBoardingImage(boardingImages[position]);
    }

    @Override
    public int getCount() {
        return boardingImages.length;
    }



    public void setBoarding(ImageSwipe.BoardingImage[] boardingImages) {
        this.boardingImages=boardingImages;

    }
}
