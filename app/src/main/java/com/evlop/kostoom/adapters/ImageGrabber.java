package com.evlop.kostoom.adapters;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.evlop.kostoom.Constants;
import com.evlop.kostoom.components.AlertDialogActions;
import com.evlop.kostoom.components.KostoomAlertDialog;
import com.evlop.kostoom.fragmets.BusinessSewing;
import com.evlop.kostoom.model.Image;

import java.util.ArrayList;

/**
 * Created by rh on 12/13/16.
 */
public class ImageGrabber implements android.widget.AdapterView.OnItemClickListener {
    private final ArrayList<Image> receiver;
    BusinessSewing businessSewing;
    GridView gridView;
    public ImageGrabber(BusinessSewing businessSewing, ArrayList<Image> receiver, GridView gridView) {
        this.businessSewing=businessSewing;
        this.receiver=receiver;
        this.gridView=gridView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

        if(position>=receiver.size()) {
            businessSewing.selectImage();
            businessSewing.imageReceiver = receiver;
        }else {
            KostoomAlertDialog dialog=new KostoomAlertDialog().setActivity(businessSewing.getActivity());
            dialog.setMessage("Are you sure you want to remove the fabric image?");
            dialog.setActions(new AlertDialogActions() {
                @Override
                public void onPositiveAction() {
                    receiver.remove(position);
                    gridView.invalidateViews();
                }

                @Override
                public void onNegativeAction() {

                }
            });
            dialog.show();
        }
        businessSewing.gridView=gridView;
    }
}
