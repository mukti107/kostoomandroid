package com.evlop.kostoom.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.model.Image;
import com.evlop.kostoom.model.Product;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by rh on 12/10/16.
 */
public class ImageUploadAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    ArrayList<Image> images;
    private Context context;
    private boolean uploadable=true;

    public ImageUploadAdapter(Context context, ArrayList<Image> images){
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context=context;
        this.images=images;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Product getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View imageView=inflater.inflate(R.layout.upload_image,null);
        ImageView image=(ImageView) imageView.findViewById(R.id.image);

        imageView.findViewById(R.id.close).setVisibility(View.GONE);

        if(position<images.size()) {
            image.setImageBitmap(images.get(position).getBitmap());
            if(uploadable){
                imageView.findViewById(R.id.close).setVisibility(View.VISIBLE);
            }
        } else if(!uploadable){
            image.setImageBitmap(null);
        }

        return imageView;
    }


    public ImageUploadAdapter setUploadable(boolean uploadable) {
        this.uploadable = uploadable;
        return this;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
