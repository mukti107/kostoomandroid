package com.evlop.kostoom.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.evlop.kostoom.R;
import com.evlop.kostoom.functions.Util;
import com.evlop.kostoom.model.SewingOrder;
import com.michael.easydialog.EasyDialog;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by rachindrapoudel on 12/18/16.
 */
public class OrderAdapter extends BaseAdapter implements View.OnClickListener {
    private final Context context;
    private final LayoutInflater inflater;
    private final List<SewingOrder> orders;
    ImageButton options;

    public OrderAdapter(Context context, List<SewingOrder> orders) {
        this.context=context;
        this.orders=orders;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public SewingOrder getItem(int i) {
        return orders.get(i) ;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View order=inflater.inflate(R.layout.order_item,null);
        ((TextView)order.findViewById(R.id.name)).setText(getItem(i).getItem().getName());
        ((TextView)order.findViewById(R.id.quantity)).setText(getItem(i).getQuantity()+" Pieces");
        ((TextView)order.findViewById(R.id.rate)).setText("Rp. "+Util.cf.format(getItem(i).getRateWithTransactionDiscount())+"/pieces");
        int furingPrice = getItem(i).getFuringPriceWithTransactionDiscount();
        ((TextView)order.findViewById(R.id.price)).setText(
                Html.fromHtml(((furingPrice>0)?"Furing Price: "+Util.cf.format(furingPrice)+"<br/>":"")+
                        "Sub total: Rp. "+Util.cf.format(getItem(i).getNetPriceWithTransactionDiscount()+getItem(i).getFuringPriceWithTransactionDiscount())
        ));
        ImageView itemImage=(ImageView)order.findViewById(R.id.image);
        itemImage.setImageBitmap(getItem(i).getImages().get(0).getBitmap());
        options= (ImageButton) order.findViewById(R.id.options);
        options.setOnClickListener(this);
        //ImageLoader.getInstance().displayImage(getItem(i).getItem().getImageUrl(),itemImage);
        return order;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public void onClick(View view) {
        Log.i("click","clicked");
        FragmentActivity activity=(FragmentActivity)context;
        View menuLayout= activity.getLayoutInflater().inflate(R.layout.cart_option_menu,null);


        new EasyDialog(context)
                .setLayout(menuLayout)
//                .setBackgroundColor(activity.getResources().getColor(R.color.blueLight))
                .setLocationByAttachedView(options)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationAlphaShow(300, 0.3f, 1.0f)
                .setAnimationAlphaDismiss(300, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(false)
                .setMarginLeftAndRight(5, 5)
//                .setGravity((halfHeight<a[1])?EasyDialog.GRAVITY_TOP:EasyDialog.GRAVITY_BOTTOM)
                .show();
    }
}
