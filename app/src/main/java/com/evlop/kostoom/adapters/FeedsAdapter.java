package com.evlop.kostoom.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.evlop.kostoom.MainActivity;
import com.evlop.kostoom.R;
import com.evlop.kostoom.functions.HttpTask;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rachindrapoudel on 12/19/16.
 */
public class FeedsAdapter extends BaseAdapter {
    private final JSONArray feeds;
    private final LayoutInflater inflater;
    MainActivity context;
    public FeedsAdapter(MainActivity context, JSONArray feeds) {
        this.inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context=context;
        this.feeds=feeds;
    }

    @Override
    public int getCount() {
        return feeds.length();
    }

    @Override
    public JSONObject getItem(int i) {
        try {
            return feeds.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View feed=inflater.inflate(R.layout.feed_list_item,null);
        TextView name=(TextView)feed.findViewById(R.id.name);
        ImageView image=(ImageView) feed.findViewById(R.id.image);
        TextView status=(TextView)feed.findViewById(R.id.status);
        TextView quantity=(TextView)feed.findViewById(R.id.quantity);
        TextView date=(TextView)feed.findViewById(R.id.date);
        try {
            name.setText(getItem(i).getString("item_name"));
            status.setText(getItem(i).getString("status"));
            quantity.setText(getItem(i).getString("quantity")+" pieces");
//            ImageLoader.getInstance().displayImage(HttpTask.baseURL+"/"+getItem(i).getJSONArray("images").getString(0),image);
            //description.setText(getItem(i).getString("item_description"));
            context.imageLoader.displayImage(HttpTask.baseURL+getItem(i).getString("item_image"),image);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return feed;
    }
}
