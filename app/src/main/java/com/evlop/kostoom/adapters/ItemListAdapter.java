package com.evlop.kostoom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.evlop.kostoom.R;
import com.evlop.kostoom.functions.HttpTask;
import com.evlop.kostoom.model.Product;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rh on 12/10/16.
 */
public class ItemListAdapter extends BaseAdapter {
    private List<String> assets=new ArrayList<>();
    private LayoutInflater inflater;
    ArrayList<Product> products;
    private Context context;

    public ItemListAdapter(Context context, ArrayList<Product> products){
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            assets=Arrays.asList(context.getResources().getAssets().list("product_images"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.context=context;
        this.products=products;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Product getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View item=inflater.inflate(R.layout.list_item,null);
        Product product=getItem(position);
        ImageView itemImage= (ImageView) item.findViewById(R.id.item_image);
        TextView itemName= (TextView) item.findViewById(R.id.item_name);

        String imageName=product.getImageUrl().replace(HttpTask.baseURL+"/uploads/","");


        if(assets.contains(imageName))
            ImageLoader.getInstance().displayImage("assets://product_images/"+imageName,itemImage);
        else
            ImageLoader.getInstance().displayImage(product.getImageUrl(),itemImage);

        itemName.setText(product.getName());

        return item;
    }
}
