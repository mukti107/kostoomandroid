package com.evlop.kostoom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.evlop.kostoom.R;
import com.evlop.kostoom.functions.HttpTask;
import com.evlop.kostoom.model.Image;
import com.evlop.kostoom.model.Product;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by rh on 12/10/16.
 */
public class UrlImageDisplayAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    String[] images;
    private Context context;

    public UrlImageDisplayAdapter(Context context, String[] images){
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context=context;
        this.images=images;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Product getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View imageView=inflater.inflate(R.layout.upload_image,null);
        ImageView image=(ImageView) imageView.findViewById(R.id.image);
        image.setImageBitmap(null);
        //ImageLoader.getInstance().displayImage(HttpTask.baseURL+"/"+images[position],image);
        return image;
    }
}
