package com.evlop.kostoom.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;

import com.evlop.kostoom.Constants;
import com.evlop.kostoom.R;
import com.evlop.kostoom.Role;
import com.evlop.kostoom.fragmets.RoleSelectFragment;
import com.evlop.kostoom.fragmets.SizeChartFragment;
import com.evlop.kostoom.model.SizeChart;

/**
 * Created by rh on 12/8/16.
 */
public class RoleSelectAdapter extends FragmentPagerAdapter {
    Context context;
    View.OnClickListener listener;
    public RoleSelectAdapter(FragmentManager supportFragmentManager, Context context, View.OnClickListener clickListener) {
        super(supportFragmentManager);
        this.context=context;
        this.listener=clickListener;
    }

    @Override
    public Fragment getItem(int position) {
        return new RoleSelectFragment().with(getPageTitle(position)).setListner(listener);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:return Role.CUSTOMER.toUpperCase();
            case 1:return Role.TAILOR.toUpperCase();
            default:return Role.SUPPLIER.toUpperCase();
        }
    }
}
